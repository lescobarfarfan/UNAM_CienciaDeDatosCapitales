
# coding: utf-8

# In[7]:

import urllib
import urllib2
from bs4 import BeautifulSoup as bs
import csv
import pandas as pd



###### Create the function
def getYahoo_records(name, numRecords):
    
    
    ###### Approach 1 #######
    url = 'https://stooq.com/q/d/l/?s=' + name +  '.US&i=d'
    result = urllib2.urlopen(url).read()
    response = urllib2.urlopen(url)
    cr = csv.reader(response)

    with open('/Users/mariazamora/Downloads/prices/' + name +'.csv','wb') as nm:
        wname = csv.writer(nm)
        for i in cr:
            wname.writerow( i )

    # if 'Exceeded the daily' in result:
    # 	wExceed.writerow([name])
    # else: 
    # 	result.to_csv('/Users/mariazamora/Downloads/prices/' + name +'.csv')
    


    ###### Approach 2 #######
#     url = "https://query1.finance.yahoo.com/v7/finance/download/" + name +"?period1=1313791200&period2=1534716000&interval=1d&events=history&crumb=rmPlic/3CMc"    
#     result = pd.read_csv(url)
#     with open(name+'.csv', 'w') as fx: 
#         fx.write(result) 
    
    ###### Approach 3 #######
#     records = dict( dates=[], prics=[])   
#     url = "https://finance.yahoo.com/quote/" + name + "/history/"
#     url = "https://finance.yahoo.com/quote/" + name + "/history?period1=1377036000&period2=1534802400&interval=1d&filter=history&frequency=1d"
#     HTMLtable = bs(  urllib2.urlopen(url).read(), "lxml" ).findAll('table')[0].tbody.findAll('tr')
#     print bs(  urllib2.urlopen(url).read(), "lxml" ).findAll('table')
    
#     for row in HTMLtable:
#         if len(records) < numRecords:
#             dataPerColumn = row.findAll('td')
#             #if dataPerColumn[1].span.text  != 'Dividend': 
#             records['dates'].append( dataPerColumn[0].span.text )
#             records['prics'].append( float(dataPerColumn[4].span.text.replace(',','')) )

#     return records



listsnames=['AMZN','T','GOOG','MO','DAL','AA','AXP','DD','BABA','ABT','UA','AMAT',
            'AMGN','AAL','AIG','ALL','ADBE','GOOGL','ACN','ABBV','MT','LLY','AGN','APA','ADP',
            'APC','AKAM','NLY','ABX','ATVI','ADSK','ADM','BMH.AX','WBA','ARNA','LUV','ACAD','PANW',
            'AMD','AET','AEP','ALXN','CLMS','AVGO','EA','DB','RAI','AEM','APD','AMBA','NVS','APOL',
            'ANF','LULU','RAD','BRK.AX','ARRY','AGNC','JBLU','A','ORLY','FOLD','AZO','ATML','AN','AZN',
            'AES','GAS','BUD','ARR','BDX','AKS','AB','ACOR','CS','AFL','ADI','AEGR','ACIW','AMP','AVP',
            'AMTD','AEO','AWK','NVO','ALTR','ALK','PAA','MTU.AX','ARCC','AAP','NAT','FNMA','FAV','AIV',
            'AGIO','AEE','UBS','AVXL','ARLP','ANTM','AGU','AG','AFSI','ABC','STO','ATI','ADT','AVB','ATW',
            'ALNY','LH','AVY','AUY','ASH','ARMH','ARIA','ANR','AINV','ACXM','ACHN','ACET','ABMD','ABM','VA',
            'LIFE','ATO','ARP','AON','ADXS','ADC','APU','SAVE','AV','AKRX','ADS','ABAX','AYI','AWH','ASML',
            'AMT','ALDR','ACM','DWA','ATRS','ARW','ARI','ARG','AR','AMCC','AMC','AL','AGEN','AAN','WTR',
            'FCAU','BAH','AXAS','AVT','ALB','AIZ','SAIC','CAR','AXLL','AU','ARO','APH','ANTH','AMX','AMDA',
            'AI','ABCO','WMC','MPX.AX','JKHY','AVAV','AMKR','ALJ','ACH','GPH.AX','ERC','APPY','ANAC','AEIS',
            'Y','MTGE','CENX','ASPS','AMRN','AMPE','AMAG','ALKS','AFFX','ADES','ACAT','AAON','XLRN','VRSK',
            'VJET','OA','ATLS','APTS','APO','ALSK','ALG','AHC','ACTG','ACAS','RBA','MAA','BAM','ATHN','AT',
            'ASX','ARCO','ANET','ANCX','AIR','AF','WAB','RS','PKG','CSH','AXDX','AVHI','AVA','ATHX','ARWR',
            'ANGI','AMG','ALSN','ALGN','AKBA','AGO','AEZS','ACRX','ROK','GLPI','DNI','AZZ','ATRC','ARRS',
            'ARMK','AOS','ANFI','AMID','AMCX','ALIM','ALE','AHT','ACW','ABB','AALI.JK','SPR','SEE','RDEN',
            'PAAS','DLPH','ADAC','CBL','BBVA','AYR','APOG','ANDE','AMSC','AMRS','AMED','ALEX','ALCO','ADUS',
            'ACTA','ACST','AAWW','WMS','PAG','MDRX','KLIC','ETH','AZPN','AXE','ATNY','APRI','AMH','AME','AJG',
            'AIQ','AHGP','AGCO','AERI','ACRE','ABEO','WAL','SYT','SHLM','NOG','HTA','GIII','DAVE','CVU','BSI',
            'AWAY','ATU','ASTI','AREX','ARE','ANSS','AMNB','AMBS','ALR','AIXG','AIN','AHL','AGX','AEG','ADTN',
            'ADMS','ACLS','ABG','ZX','NXJ','KS','HOLI','GPI','ENI','BEAV','AXTA','AWR','AWI','AVEO','AUO','ATHM',
            'ATAX','ASM','AROC','ANH','ALX','AHS','AGI','AER','AE','RHHBY','PETX','ODC','NAO','KAR','HUSA','HIVE',
            'FMS','DOX','CXW','AZUR','AXS','AXL','AWX','AVID','ASNA','ASGN','ARDX','ARAY','AQXP','APT','APDN','AMBC',
            'ALGT','ALDW','AIT','AIRM','AIMC','AEL','AEHR','ADHD','ACUR','ACHC','ACFC','ACCO','ABY','TA','RPAI','MANH','LAMR','KYN','AXN','ATRO','ATNI','ARCW','APEI','AP','ANIK','ANGO','AMTG','AMSG','AMOT','AM','ALV','ALOG','AKR','AEGN','ADS.DE','ZLTQ','WRLD','UHAL','UAMY','SAH','RJET','NAII','AQNM','CAS','CACC','ATSG','ASEI','ASB','ARTX','AROW','ARCB','AMRK','ALRM','AHP','AGRX','AFAM','ADK','ACSF','ABTL','ABGB','ABEV','ABCD','AAOI','USAP','STFC','STAY','SEED','RGA','IDSA','HART','CH','CEA','BREW','AXR','AVG','AVD','AUDC','ATRI','ATOS','ARC','APIC','AOSL','AOI','AMWD','ALXA','ALLY','AIRI','AFOP','ACGL','ACFN','ABR','ABCB','SAMG','REXI','RAIL','NSAM','MITT','LCM','HASI','GOL','GIL','EAD','ATTO','ATR','ATNM','ASTC','ASR','ASC','ARTNA','ARGS','AOD','AGNCP','ADRO','ACNB','AAV','AAT','ZNH','UAM','NTT','NFJ','LNT','KALU','HOMB','HIX','FAF','FAC','EGT','CAAS','BGR','BETR','AUPH','ATV','ATLC']

print "download"
print len(listsnames)

with open('TickReady.csv','wb') as f:
    wReady = csv.writer(f)

    with open('TickNotReady.csv','wb') as f2:
        wNotReady = csv.writer(f2)

        with open('TickExceeded.csv','wb') as f3:

            wExceed = csv.writer(f3)
		    #### Header
		    # wReady.writerow( ('Tickr','Dates','Prics') )
            for name in listsnames:
		        print name
		        try:
		            
			        ###### Call the function 
			        sampleP = getYahoo_records(name, 600)
			        # for i in range(252):
			        #     wReady.writerow( (name,sampleP['dates'][i],sampleP['prics'][i]) )
			        wReady.writerow( [name] )

		        except:

		        	wNotReady.writerow( [name] )


# In[ ]:



