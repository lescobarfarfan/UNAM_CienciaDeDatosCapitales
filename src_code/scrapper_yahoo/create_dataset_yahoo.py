import pandas as pd
import json
from os import listdir
from os.path import isfile, join

mypath="/Users/alimon/Documents/Scrapper/output/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles= onlyfiles[1:]


ticker=[]
address=[]
city=[]
country=[]
industry=[]
longBusinessSummary=[]
sector=[]
state=[]
website=[]
fullTimeEmployee=[]
zip=[]

for file in onlyfiles:
    ticker.append(file.replace('.json',''))
    with open(mypath + file) as f:
        data = json.load(f)
        keys_lvl_1 = data.keys()
        if 'quoteSummary' in keys_lvl_1 and 'result' in data['quoteSummary'].keys() and data['quoteSummary']['result'] and\
        'summaryProfile' in data['quoteSummary']['result'][0].keys():
            keys_lvl_4 = data['quoteSummary']['result'][0]['summaryProfile'].keys()
            if 'address1' in keys_lvl_4:
                address.append(data['quoteSummary']['result'][0]['summaryProfile']['address1'])
            else:
                address.append('')
            if 'city' in keys_lvl_4:
                city.append(data['quoteSummary']['result'][0]['summaryProfile']['city'])
            else:
                city.append('')
            if 'country' in keys_lvl_4:
                country.append(data['quoteSummary']['result'][0]['summaryProfile']['country'])
            else:
                country.append('')
            if 'industry' in keys_lvl_4:
                industry.append(data['quoteSummary']['result'][0]['summaryProfile']['industry'])
            else:
                industry.append('')
            if 'longBusinessSummary' in keys_lvl_4:
                longBusinessSummary.append(data['quoteSummary']['result'][0]['summaryProfile']['longBusinessSummary'])
            else:
                longBusinessSummary.append('')
            if 'sector' in keys_lvl_4:
                sector.append(data['quoteSummary']['result'][0]['summaryProfile']['sector'])
            else:
                sector.append('')
            if 'website' in keys_lvl_4:
                website.append(data['quoteSummary']['result'][0]['summaryProfile']['website'])
            else:
                website.append('')
            if 'fullTimeEmployees' in keys_lvl_4:
                fullTimeEmployee.append(data['quoteSummary']['result'][0]['summaryProfile']['fullTimeEmployees'])
            else:
                fullTimeEmployee.append('')
            if 'state' in keys_lvl_4:
                state.append(data['quoteSummary']['result'][0]['summaryProfile']['state'])
            else:
                state.append('') 
            if 'zip' in keys_lvl_4:
                zip.append(data['quoteSummary']['result'][0]['summaryProfile']['zip'])
            else:
                zip.append('') 
                
                
        else:
            address.append('')
            city.append('')
            country.append('')
            industry.append('')
            longBusinessSummary.append('')
            sector.append('')
            state.append('')
            website.append('')
            fullTimeEmployee.append('')
            zip.append('')
   

df = pd.DataFrame({'Ticker':ticker})
df['Address']= address
df['City']= city
df['Country']= country
df['Industry']= industry
df['LongBusinessSummary']= longBusinessSummary
df['Sector']= sector
df['State']= state
df['Website']= website
df['fullTimeEmployee']= fullTimeEmployee
df['zip']= fullTimeEmployee

yahoo_ticker= pd.read_csv('/Users/alimon/Documents/Github/2018_UNAM_CienciaDeDatosCapitales/Dataset/yahoo/Yahoo_tickers.csv')
yahoo_ticker = yahoo_ticker[['Ticker', 'Exchange', 'Name']]

df = pd.merge(df, yahoo_ticker, how='left', on='Ticker')

df.to_csv('/Users/alimon/Documents/dataset/Yahoo/dataset.csv', encoding='utf-8-sig', index=False)


