
# coding: utf-8

# In[2]:

import pandas as pd
import json
from os import listdir
from os.path import isfile, join

onlyfiles = [f for f in listdir('output/') if isfile(join('output/', f))]

onlyfiles


# In[14]:


ticker=[]
address=[]
city=[]
country=[]
industry=[]
longBusinessSummary=[]
sector=[]
state=[]
website=[]
fullTimeEmployee=[]
zip=[]

currentPrice=[]
ebitda=[]
totalRevenue=[]
totalDebt=[]

sharesOutstanding=[]
fiveYearAverageReturn=[]
SandP52WeekChange=[]
quoteYield=[]
quote52WeekChange=[]

for file in onlyfiles:
	if '.json' in file:
	    ticker.append(file.replace('.json',''))
	    with open('output/' + file) as f:
	    	#print file
	        data = json.load(f, encoding='utf-8')
	        keys_lvl_1 = data.keys()
	        
	        ############################ PROFILE ###################################
	        
	        if 'quoteSummary' in keys_lvl_1 and 'result' in data['quoteSummary'].keys() and data['quoteSummary']['result'] and 'summaryProfile' in data['quoteSummary']['result'][0].keys():
	            keys_lvl_4 = data['quoteSummary']['result'][0]['summaryProfile'].keys()
	            if 'address1' in keys_lvl_4:
	                address.append(data['quoteSummary']['result'][0]['summaryProfile']['address1'])
	            else:
	                address.append('')
	            if 'city' in keys_lvl_4:
	                city.append(data['quoteSummary']['result'][0]['summaryProfile']['city'])
	            else:
	                city.append('')
	            if 'country' in keys_lvl_4:
	                country.append(data['quoteSummary']['result'][0]['summaryProfile']['country'])
	            else:
	                country.append('')
	            if 'industry' in keys_lvl_4:
	                industry.append(data['quoteSummary']['result'][0]['summaryProfile']['industry'])
	            else:
	                industry.append('')
	            if 'longBusinessSummary' in keys_lvl_4:
	                longBusinessSummary.append(data['quoteSummary']['result'][0]['summaryProfile']['longBusinessSummary'])
	            else:
	                longBusinessSummary.append('')
	            if 'sector' in keys_lvl_4:
	                sector.append(data['quoteSummary']['result'][0]['summaryProfile']['sector'])
	            else:
	                sector.append('')
	            if 'website' in keys_lvl_4:
	                website.append(data['quoteSummary']['result'][0]['summaryProfile']['website'])
	            else:
	                website.append('')
	            if 'fullTimeEmployees' in keys_lvl_4:
	                fullTimeEmployee.append(data['quoteSummary']['result'][0]['summaryProfile']['fullTimeEmployees'])
	            else:
	                fullTimeEmployee.append('')
	            if 'state' in keys_lvl_4:
	                state.append(data['quoteSummary']['result'][0]['summaryProfile']['state'])
	            else:
	                state.append('') 
	            if 'zip' in keys_lvl_4:
	                zip.append(data['quoteSummary']['result'][0]['summaryProfile']['zip'])
	            else:
	                zip.append('') 
	                
	                
	        else:
	            address.append('')
	            city.append('')
	            country.append('')
	            industry.append('')
	            longBusinessSummary.append('')
	            sector.append('')
	            state.append('')
	            website.append('')
	            fullTimeEmployee.append('')
	            zip.append('')

	            
	        ############################ FINANCIAL ############################
	        
	        if 'quoteSummary' in keys_lvl_1 and 'result' in data['quoteSummary'].keys() and data['quoteSummary']['result'] and  'financialData' in data['quoteSummary']['result'][0].keys():
	            keys_lvl_4 = data['quoteSummary']['result'][0]['financialData'].keys()
	            if 'currentPrice' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['financialData']['currentPrice'] != {}:
	                    currentPrice.append(data['quoteSummary']['result'][0]['financialData']['currentPrice']['raw'])
	                else:
	                	currentPrice.append('')
	            else:
	                currentPrice.append('')
	            if 'ebitda' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['financialData']['ebitda'] != {}:
	                    ebitda.append(data['quoteSummary']['result'][0]['financialData']['ebitda']['raw'])
	                else:
	                	ebitda.append('')
	            else:
	                currentPrice.append('')
	            if 'totalRevenue' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['financialData']['totalRevenue'] != {}:
	            		totalRevenue.append(data['quoteSummary']['result'][0]['financialData']['totalRevenue']['raw'])
	            	else: 
	            		totalRevenue.append('')
	            else:
	                totalRevenue.append('')
	            if 'totalDebt' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['financialData']['totalDebt'] != {}:
	            		totalDebt.append(data['quoteSummary']['result'][0]['financialData']['totalDebt']['raw'])
	            	else: 
	            		totalDebt.append('')
	            else:
	                totalRevenue.append('')
	        else:
	            currentPrice.append('')
	            ebitda.append('')
	            totalRevenue.append('')
	            totalDebt.append('')
	            
	            
	        ############################ statistics ############################
	        
	        if 'quoteSummary' in keys_lvl_1 and 'result' in data['quoteSummary'].keys() and data['quoteSummary']['result'] and  'defaultKeyStatistics' in data['quoteSummary']['result'][0].keys():
	            keys_lvl_4 = data['quoteSummary']['result'][0]['defaultKeyStatistics'].keys()
	            if 'sharesOutstanding' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['defaultKeyStatistics']['sharesOutstanding'] != {}:
	                    sharesOutstanding.append(data['quoteSummary']['result'][0]['defaultKeyStatistics']['sharesOutstanding']['raw'])
	                else:
	                    sharesOutstanding.append('')
	            else:
	                sharesOutstanding.append('')
	            if 'fiveYearAverageReturn' in keys_lvl_4:
	                fiveYearAverageReturn.append(data['quoteSummary']['result'][0]['defaultKeyStatistics']['fiveYearAverageReturn'])
	            else:
	                fiveYearAverageReturn.append('')
	            if 'SandP52WeekChange' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['defaultKeyStatistics']['SandP52WeekChange'] != {}:
	                    SandP52WeekChange.append(data['quoteSummary']['result'][0]['defaultKeyStatistics']['SandP52WeekChange']['raw'])
	                else:
	                    SandP52WeekChange.append('')
	            else:
	                SandP52WeekChange.append('')
	            if 'yield' in keys_lvl_4:
	                quoteYield.append(data['quoteSummary']['result'][0]['defaultKeyStatistics']['yield'])
	            else:
	                quoteYield.append('')
	            if '52WeekChange' in keys_lvl_4:
	            	if data['quoteSummary']['result'][0]['defaultKeyStatistics']['52WeekChange'] != {}:
	                    quote52WeekChange.append(data['quoteSummary']['result'][0]['defaultKeyStatistics']['52WeekChange']['raw'])
	                else:
	                    quote52WeekChange.append('')
	            else:
	                quote52WeekChange.append('')
	        else:
	            sharesOutstanding.append('')
	            fiveYearAverageReturn.append('')
	            SandP52WeekChange.append('')
	            quoteYield.append('')
	            quote52WeekChange.append('')

	                
                
### profile
df = pd.DataFrame({'Ticker':ticker})
df['Address']= address
df['City']= city
df['Country']= country
df['Industry']= industry
df['LongBusinessSummary']= longBusinessSummary
df['Sector']= sector
df['State']= state
df['Website']= website
df['fullTimeEmployee']= fullTimeEmployee
df['zip']= fullTimeEmployee
### financial
df['currentPrice']= currentPrice
df['ebitda']= ebitda
df['totalRevenue']= totalRevenue
df['totalDebt']= totalDebt
### statistics
df['sharesOutstanding']= sharesOutstanding
df['fiveYearAverageReturn']= fiveYearAverageReturn
df['SandP52WeekChange']= SandP52WeekChange
df['quoteYield']= quoteYield
df['quote52WeekChange']= quote52WeekChange

yahoo_ticker= pd.read_csv('data_outliers/SandP.csv', encoding='latin-1')
yahoo_ticker = yahoo_ticker[['Ticker symbol','Security']]
yahoo_ticker.columns = ['Ticker','Security']

df = pd.merge(df, yahoo_ticker, how='left', on='Ticker')

def changeStr(x):
	if isinstance(x, basestring): 
		return x.encode('unicode-escape').decode('utf-8-sig')
	else:
		return x

for i in df.columns:
	print i
	df[i] = df[i].map(lambda x: changeStr(x))

df.to_csv('data_outliers/globalCompanies_sandp.csv', encoding='utf-8-sig', index=False)



# In[ ]:



