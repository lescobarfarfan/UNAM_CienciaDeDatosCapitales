# -*- coding: utf-8 -*-
"""
Spyder Editor
This is a temporary script file.
"""
import wptools
import json
import pandas as pd
import csv
import requests
from bs4 import BeautifulSoup

organizations= pd.read_csv('dataset/organizations.csv')
company_name=organizations['company_name']
list_companies=list(company_name)
print len(list_companies)   # 676831
#https://cims.nyu.edu/webapps/content/systems/resources/computeservers
 
validURLchar = list("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=")
nstartRange=0
nendRange=nstartRange+50000

############################## page id #################################################

with open('exceptions_pageid.csv','wb') as exceFile:
    with open('pageId.csv','wb') as pageidFile:
        pageidWriter = csv.writer(pageidFile)
        pageidWriter.writerow( ['organization_Num','organization_Name','wiki_pageid','wikidata_available'] )
        exceWriter = csv.writer(exceFile)
        exceWriter.writerow( ['organization_Num','organization_Name','errorInfo'] )

        for i in range(nstartRange,nendRange):
            print i
            try:

                varTest= wptools.page(list_companies[i])
                pageid = varTest.get_parse().pageid
                pageget = wptools.page(list_companies[i]).get().wikidata.keys()

                pageidWriter.writerow( [i,list_companies[i],pageid,str(pageget)] )

            except LookupError as e:
                errorCode = requests.get(str(e)+'&format=json').json()[u'error'][u'code']
                exceWriter.writerow( [i,list_companies[i],errorCode] )


############################## infobox #################################################


with open('exceptions_infobox.csv','wb') as exceFile:
    with open('infobox.csv','wb') as infoboxFile:
        infoboxWriter = csv.writer(infoboxFile)
        infoboxWriter.writerow( ['organization_Num','organization_Name','fileName'] )
        exceWriter = csv.writer(exceFile)
        exceWriter.writerow( ['organization_Num','organization_Name','errorInfo'] )

        for i in range(nstartRange,nendRange):
            try:
                page = wptools.page(list_companies[i], timeout=5).get_parse()
                infobox = page.infobox

                if infobox != 'null':
                    filename = str(i) + "_" + list_companies[i].replace(' ','%20')+'.json'
                    infoboxWriter.writerow( [i,list_companies[i], filename] )
                    with open('output/'+filename, 'w') as outfile:
                        json.dump(infobox, outfile)
                else:
                    exceWriter.writerow( [i,list_companies[i],'nullinfobox'] )

            except LookupError as e:
                errorCode = requests.get(str(e)+'&format=json').json()[u'error'][u'code']
                exceWriter.writerow( [i,list_companies[i],errorCode] )
            except:
                infoboxWriter.writerow( [i,list_companies[i], 'notincluded_urlcharacter'] )


