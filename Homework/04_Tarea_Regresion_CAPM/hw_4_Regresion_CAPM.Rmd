---
title: "Tarea: Regresión Lineal y CAPM"
author: "Maria Leonor y Ali Josue"
date: "Entrega - Nov 26, 2018"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


<br/ >


# Introducción a CAPM (Capital Asset Pricing Model)

<br/ >

Modelo usado en finanzas corporativas que incluye los siguientes conceptos:

- _Expected returns_
- _Market risk (systematic) vs Stock specific risk_
- _Asset Prices_
- _Exposure_

En un mundo ideal, esperaríamos que los precios de los instrumentos en el mercado estuvieran en equilibrio con respecto a la oferta y demanda existente. Entonces, si conocemos los totales de demanda y oferta en una acción, deberíamos poder encontrar su **precio de mercado y el retorno esperado**. ¿Cómo podemos evaluar la demanda de nuestras acciones entonces? 


<br/ >

**Supuestos principales para CAPM**

<br/ >

1. Todos los inversionistas son **racionales** y tienen preferencias de **media-varianza**. Esto significa que eligen inversiones haciendo un trade-off entre el riesgo vs el rendimiento que desean. En particular, buscan distribuciones más cercanas a la media y con menor varianza. 

2. Todos los inversionistas tienen las mismas ideas con respecto a **retornos, volatilidad y correlaciones**. 


Estos conceptos corresponden a la teoría conocida como **Modern portfolio theory** (_Harry Markowitz - Portfolio Selection_), cuyos fundamentos establecen que los inversionistas **maximizan la utilidad, prefieren mayores cantidades y tienden a ser adversos al riesgo**.

<br/ >

Con estos dos elementos es suficiente para decir que, a nivel general, la demanda de la que hablamos está conformada por **portafolios eficientes** que tienen **retornos maximizados en niveles de riesgo especificos**. Por ende, se esperaría que todos los precios en el mercado se ajustarán hasta igualar la oferta de todos los componentes del portafolio en riesgo. Esto se conoce como **_"el portafolio de mercado" (market portfolio)_** y se usa por los inversionistas racionales, **variando únicamente los pesos del portafolio y los instrumentos libres de riesgo**. ¿Podríamos modelar esta relación? Para eso existe CAPM. De hecho, para CAPM se trabaja con un equilibrio que está correlacionado con el riesgo de todos los activos financieros (market risk) en el portafolio de mercado. 

<br />

¿Qué otros supuestos existen?

3. Las inversiones están limitadas solo a activos financieros diversificados y disponibles de manera pública, donde las deudas (obtenidas y otorgadas) están expuestas a una **tasa libre de riesgo**.

4. No se considera el pago de impuestos ni costos de transacción por las operaciones. 




<br/ >

**Fórmula de la relación / Regresión Lineal**

<br /> 

$$(StockReturn \; - \; RiskFreeRate) \; = \; w_0 \; + \; w1 \;(MarketReturn \; -\; RiskFreeRate) \; + \;error $$


$$r_i - r_f \; = \; w_0 \; + \; w_1 \; (r_{mkt} - r_f) \; + \; \epsilon_i $$


<br/ >

**CAPM fórmula**

$$E[r_i] - r_f \; = \; \alpha_i \; + \; \beta_i \; (E[r_{mkt}] - r_f) \; + \; \epsilon_i $$



$$ E[r_i] = r_f \; + \; \beta_i \; (E[r_{mkt}] - r_f) \; + \; \epsilon_i $$

<br/ >


$$ ¿Qué \; sucedió \; con  \; \alpha  \;? $$
<br />


```{r invest, out.width = "550px", echo=FALSE, fig.align = "center", fig.cap="\\label{fig:invest}Source: [Alpha & Beta - Evestment - Investment Statistics Guide](https://www.evestment.com/resources/investment-statistics-guide/alpha-and-beta/#!)"}
knitr::include_graphics("regression.jpg")
```



<br/ >


# Aspectos relevantes e intuición del modelo

<br/ >


1. **Beta $\beta$**: es el principal valor de exposición y corresponde a la pendiente (coeficiente) de un modelo de regresión lineal. 
    
    La mayor parte del riesgo del que hablamos en este fundamento se captura con este coeficiente que mide el **retorno en exceso esperado vs el retorno en exceso del portafolio de mercado**. En particular, mide el exceso _esperado_ por cada 1% de exceso _esperado_ del mercado, es decir, buscamos capturar la correlación del activo vs el mercado.
    
    
    <br/ >
    
    **Conceptos de Beta para no olvidar en CAPM**
    
    
    Beta = Systematic risk (non-diversifiable relative risk, it has to be hedged) 
    
    
    Beta = Measure of risk **exposure** for an asset
    
    
    Beta = Movement of a stock vs market movement 
    
    
    Beta = 0 means an asset effectively riskFree
    
    
    Beta = Covariance ( stock_returns, market_returns ) / Var (market_returns)
    
    $$\beta_i = \frac{Cov(r_i \, , \, r_{mkt})}{Var(r_{mkt})} $$
    
     <br/ >
    
    * Beta=1 as risky as the market.
    * Beta>1 more risky than the market (Beta of 1.2 = 20% more than market = 1.2 times the market)
    * Beta<1 less risky than the market (Beta of 0.9 = 10% less volatile = only 90% of market's g ain/loss)
    
    <br/ >
    
    Example: [Betas per sector](http://pages.stern.nyu.edu/~adamodar/New_Home_Page/datafile/totalbeta.html)



   <br/ >

2. **Alpha $\alpha$**: es el coeficiente _constante_ de nuestra regresión lineal.

    Bajo la perspectiva de CAPM, **en un mercado eficiente, este coeficiente vale cero!!**
    
    En caso de que no sea así, incluso con un muy buen retorno, vamos a poder usar este coeficiente para analizar el rendimiento (_dado un nivel de riesgo_):
    
    * Alpha < 0 rate of return underperformed the benchmark
    
    * Alpha = 0 rate of return matches the benchmark
    
    * Alpha > 0 rate of return outperformed the benchmark
   
   


<br />

3. Unsystematic risk ($\epsilon_{i,t}$)


<br />



De esta forma con CAPM, podríamos decir que el peso óptimo en riesgo de un activo financiero en el portafolio de mercado corresponde a:

$$ w_{stock} = \frac {MktCap_{stock}} {MktCap_{Allstocks}} =  \, proportion \, per \,stock \,in \, market\, portfolio$$


<br />


# Modelos más complejos

<br />

Con base en el modelo de CAPM, se han creado otros modelos, conocidos como "factor models" (_a.k.a. multifactor models_). Uno de los más importantes es el de **Fama-French three-factor model**, iniciado por Eugene Fama y Kenneth French. En este modelo ya se consideran otras variables para la predicción, no solo el componente de exposición al mercado (_market risk_), también el impacto de otros dos factores que marcan la diferencia entre compañias grandes vs chicas (con respecto al _market cap_ y al valor contable en libros _"book-to-market ratio"_ [^1]). De eso se trata esta tarea, de analizar otras variables.


<br/ >
    
    
# Homework: CAPM in R & Alternative Data


   <br/ >

1. **Baseline: CAPM basics**
    
    a. Stock Returns: 3 years of **monthly** data from a stock incorporated in the US and [listed on the NYSE, AMEX or NASDAQ](https://www.nasdaq.com/screening/company-list.aspx)
    
    b. MarketReturns - RiskFree[^2]: excess return on the market ([value-weight return](http://www.crsp.com/indexes-pages/returns-and-constituents) of all CRSP firms  [share code 10 or 11](http://www.crsp.com/products/documentation/data-definitions-1))
    
    c. Simple Linear Regression: Basic CAPM
    
    <br/ >
       
2. **Fama-French three-factor**: 

    a. Stock Returns (baseline)
    
    b. Fama-French Features (Kenneth R. French website):
    
        i. Extract monthly $ExcessReturn$
        
        ii. Extract monthly $SMB$ (Small Minus Big): Market cap' assumption.
        
        iii. Extract monthly $HML$ (High Minus Low): Book-to-market ratio assumption
        
        iv. Extract monthly $R_f$ risk-free rate of interest: One-month Treasury bill rate
        
    c. Multiple linear regression: Forward Selection

     <br/ >
  
3. **Fama-French three-factor + Alternative Data**:


    a. Stock Returns (baseline)
    
    b. Fama-French Features
    
    c. G-trends foundations per month[^3]
    
        i. Integer of interest for "NYSE"" per month
        
        ii. Integer of interest for "NASDAQ" per month
        
        iii. Integer of interest for "small caps"  per month[^4]
        
        iv. Integer of interest for "value stocks"  per month[^5]
        
        v. Integer of interest for the stock's TICKER per month
    
    d. Multiple linear regression with and without penalizations (ridge and lasso)
    
    
     <br/ >

4. **For all models: Compare performace, plot, explain relevance and coefficients per variable**

    - Don't forget general considerations (if necessary): Transformations (e.g. Log), Normalization, Sample split (Train, test, Cross-val) ,....

<br/ >



# Reference

<br/ >

- _"Under CAPM: all variance means risk, but not all risk will be rewarded (i.e. only variability related to market variability will be rewarded)."_ 
 
- _[Fama and French, 1993, "Common Risk Factors in the Returns on Stocks and Bonds," Journal of Financial Economics](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.139.5892&rep=rep1&type=pdf)_

- _[Eugene F. Fama and Kenneth R. French, 2004, The Capital Asset Pricing Model:Theory and Evidence](http://www-personal.umich.edu/~kathrynd/JEP.FamaandFrench.pdf)_

<br/ >

[^1]: Accounting value (historical cost) vs Market value in all stock market, on the market cap assumption.

[^2]: Kenneth R. French assumption [Documentation](http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/Data_Library/f-f_factors.html) and data [data source](http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html)


[^3]: Gtrends [package and documentation](https://cran.r-project.org/web/packages/gtrendsR/gtrendsR.pdf)

[^4]: _Fama-French three-factor:_ A portfolio with more small-cap' companies should outperform (a.k.a. "size effect")

[^5]: _Fama-French three-factor:_ When there is high book-to-market ratio we usually say 'value stocks' and they tend to outperform compared to lower book-to-market (a.k.a. growth stocks)


