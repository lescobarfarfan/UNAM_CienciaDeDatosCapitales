---
title: "Tarea 3: SOLUCIONES"
author: "Maria L Zamora y Ali Limon"
due date: "6 Oct, 2018. 11:59 p.m."
output: html_document
---

<style>
p.caption {
  font-size: 0.78em;
}
</style>


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Soluciones de la tarea

```{r}
### Libraries
suppressMessages(library(rvest))
suppressMessages(library(curl))
suppressMessages(library(dplyr))
suppressMessages(library(ggplot2))
```



<br />

- Select top 20 stocks Companies Weighted from largest to smallest  (S&P500)

    + https://www.slickcharts.com/sp500 

```{r}
location <- 'dataSol/'
top20 <- read.csv(paste0(location,'sp500_CompaniesWeight.csv'))
knitr::kable(top20, format = "markdown")
```


<br /> 

- Web crawling: Get news and descriptions per stock (ticker)  -> SAVE ALL DATA IN TXT FILES

    + https://cran.r-project.org/web/packages/rvest/index.html 
    
    
En la siguient funcion de R vamos a ver el mismo codigo en R de la clase de web crawling, sólo agregamos un poco más de limpieza a los textos


```{r}
webNews <- function(baseURL, portURL, keyword, filename){
  
    ### Local var: baseURL, portURL, keyword, filename
    
    ### Create a list (dictionary) of news 
    newsData <- vector(mode = 'list', length=length(top20$Symbol))
    names(newsData) <- top20$Symbol
    newsData
    
    ### Get the HTML data
    for (i in top20$Symbol)
    {
      WEBLINK <- paste0(baseURL,i,portURL)
      print (WEBLINK)
      content <- read_html(curl(WEBLINK, handle = curl::new_handle("useragent" = "Mozilla/5.0")))
      
      most_read <- content  %>% 
                  html_nodes(keyword) %>% 
                  html_text()
      
      newsData[[i]][[1]] <- most_read
      
      date_time<-Sys.time()
      while((as.numeric(Sys.time()) - as.numeric(date_time))<2){}
    }
    
    #newsData
    
    ### Transform into dataframe and put it into a txt file
    fn <- paste0(location,filename,'.txt')
    if (file.exists(fn)) file.remove(fn)
    for (stock in names(newsData))
    {
      print (stock)
      for (headline in newsData[[stock]][[1]]) {
        headline <- sub("\n", "", headline, fixed = TRUE)
        headline <- sub("|", "", headline, fixed = TRUE)
        headline <- gsub("[\r\n\"]", "", headline)
        headline <- gsub('[[:punct:] ]+',' ',headline)
        row <- data.frame(stock, headline)
        write.table(row, file = paste0(location,filename,'.txt'), sep = "|", append = TRUE, quote = FALSE, col.names = FALSE, row.names = FALSE)
        
      }
    }
    

}

```

Ahora vamos a consultar en la página de Reuters las noticias con base en el Ticker. Se va a crear un archivo llamado "textWSJ". NOTA: estamos iterando con base el la lista de Tickers (**ver en la funcion: Top20$Symbol**) y estamos buscando la palabra ".feature" en el codigo fuente.

```{r}
tempURL <- 'https://www.reuters.com/finance/stocks/company-news/'
tempPostURL <- ''
searchWord <- ".feature"
fileNew <- "textReuters"

##### We only run this once because otherwise it takes forever

#webNews(tempURL, tempPostURL,searchWord, fileNew)
```



<br />

- Create a dataframe: You might create a dataframe with columns: news_id, ticker, text.


Con el siguiente código leemos el archivo creado.
  
  
```{r}
newsFile <- read.csv(paste0(location,fileNew,'.txt'),sep='|',header=FALSE, stringsAsFactors=FALSE)
names(newsFile) <- c('Stock','News')

knitr::kable(head(newsFile), format = "markdown")
```

<br />

- Build the corpus: Create a corpus using news as documents and use the one-token-per-document-per-row structure to represent your data in R.  

```{r}
########## Tokenization

#install.packages("tidytext")
suppressMessages(library(tidytext))
#install.packages("stringr")
suppressMessages(library(stringr))
suppressMessages(library(wordcloud))

newsFile_token <- newsFile %>% unnest_tokens( word, News, strip_punct =TRUE)
head(newsFile_token, 30)
nrow(newsFile_token)
```



<br />

- Preprocessing: remove stopwords and punctuation, turn to lowercase. Finally get frequency of words (tf) and the term frequency–inverse document frequency (tf-idf). 

    + https://gist.github.com/bindiego/db4b820fcd2c5eef5fa5
    
```{r}
########## Clean and count

data(stop_words)
ourWords <- c('stocks','stock','sept')
newsFile_CompanyTokens <- unique( strsplit(do.call(paste,as.list(top20$Company)) , " ")[[1]] )
newsFile_CompanyTokens <- gsub("\\.", "", newsFile_CompanyTokens)
newsFile_CompanyTokens <- as.list(tolower(newsFile_CompanyTokens))

newsFile_token_new <- filter(newsFile_token, !word %in% stop_words$word)
newsFile_token_new <- filter(newsFile_token_new, !word %in%  ourWords)   ##Already lower case
newsFile_token_new <- filter(newsFile_token_new, !word %in%  newsFile_CompanyTokens)  ##No company names
newsFile_token_new <- filter(newsFile_token_new, !str_detect(word, "\\d"))
head(newsFile_token_new , 30)
nrow(newsFile_token_new )
```


    
<br /> 

- Get the top 10 words (based on tfidf)   

Qué pasa sin TF-IDF?

```{r}
newsFile_token_new  %>%
  count(word, sort = TRUE) %>%
  filter(n > 20) %>%
  mutate(word = reorder(word, n)) %>%
  ggplot(aes(word, n)) +
  geom_col() +
  xlab(NULL) +
  coord_flip()
```

Qué pasa con TF-IDF?

```{r}
counts_newsFile_token_clean <- newsFile_token_new %>% count(Stock, word, sort = TRUE)
newsFile_token_clean_tfidf <- as.data.frame(counts_newsFile_token_clean %>% bind_tf_idf(word, Stock, n))
newsFile_token_clean_tfidf <- newsFile_token_clean_tfidf %>% arrange(desc(tf_idf))
head(newsFile_token_clean_tfidf, 15)
nrow(newsFile_token_clean_tfidf)
```


```{r}
newsFile_token_clean_tfidf %>%
  arrange(desc(tf_idf)) %>%
  filter(tf_idf>.1) %>%
  mutate(word = reorder(word, tf_idf)) %>%
  ggplot(aes(word, tf_idf)) +
  geom_col() +
  xlab(NULL) +
  coord_flip()

```

Entonces vamos a tomar las que realmente son importantes con base en TF-IDF


```{r}
hist(newsFile_token_clean_tfidf$tf_idf)
hist(log10(newsFile_token_clean_tfidf$tf_idf))
```

```{r}
newsFile_token_clean <- newsFile_token_clean_tfidf %>% filter(tf_idf>.01)
```


<br /> 

- Get the sentiment of each word

    + https://cran.r-project.org/web/packages/syuzhet/syuzhet.pdf
    + https://www.tidytextmining.com/sentiment.html
    
```{r}
########## Sentiment analysis

#install.packages("syuzhet")
library(syuzhet) # for sentiment analysis

uniqueWords <-  unique(newsFile_token_clean$word)
sentiments <- sapply(uniqueWords, function(x) get_nrc_sentiment(as.character(x)) )
sentiments <- as.data.frame(aperm(sentiments)) 

knitr::kable(head(sentiments,10), format = "markdown")

#### To remove words now we only need numbers (adding columns later)
sentiments <- as.data.frame(lapply(sentiments, as.numeric))

```


```{r}
emotions <- data.frame("count"=colSums(sentiments[,c(1:8)]))
emotions <- cbind("sentiment" = rownames(emotions), emotions)
knitr::kable(emotions, format = "markdown")
```
    
<br /> 

- Analyze sentiment, trends and topics per STOCK: BE CREATIVE, use at least 3 different graphs and explain

```{r}
wordcloud(words = newsFile_token_clean$word, max.words=70, random.order=FALSE, colors=brewer.pal(8, "Dark2"),scale=c(2.5,.05))
```

```{r}

ggplot(data = emotions, aes(x = sentiment, y = count)) +
  geom_bar(aes(fill = sentiment), stat = "identity") +
  xlab("Sentiment") + ylab("Total Count") + 
  scale_fill_brewer(palette='RdBu') + 
  theme_bw() + theme(legend.position='none')

```

```{r}
########## Matrix and Sparsity

suppressMessages(library(stats))
suppressMessages(library(tm))

wordsDataMatrix <- newsFile_token_clean[c('Stock','word','n')] %>% cast_tdm(word, Stock, n)
wordsDataMatrix

inspect(wordsDataMatrix[1100:1150,12:18])
```

```{r}
########## Tidytext sentiment

suppressMessages(library(tidytext))

ap_td <- tidy(wordsDataMatrix)
ap_sentiments <- ap_td %>% inner_join(get_sentiments("bing"), by = c(term = "word"))

ap_sentiments

suppressMessages(library(tidyr))

ap_sentiments %>%
  count(document, sentiment, wt = count) %>%
  spread(sentiment, n, fill = 0) %>%
  mutate(sentiment = positive - negative) %>%
  arrange(sentiment)

```


```{r}
library(ggplot2)
ap_sentiments %>%
  count(sentiment, term, wt = count) %>%
  filter(n >= 5) %>%
  mutate(n = ifelse(sentiment == "negative", -n, n)) %>%
  mutate(term = reorder(term, n)) %>%
  ggplot(aes(term, n, fill = sentiment)) +
  geom_bar(stat = "identity") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  ylab("Contribution to sentiment")

```


## AAPL = Apple

```{r}
newsFile_AAPL  <- filter(newsFile_token_clean, Stock=='AAPL') %>% arrange(desc(tf_idf))
wordsFiltered <- newsFile_AAPL$word
wordsDataFiltered <- newsFile_AAPL[c('Stock','word','n')]
```

```{r echo=FALSE}
wordcloud(newsFile_AAPL$word,freq = newsFile_AAPL$n, colors=brewer.pal(8, "Dark2"), random.order=FALSE,scale=c(4,.01))
```

```{r echo=FALSE}
sentiments <- sapply(wordsFiltered, function(x) get_nrc_sentiment(as.character(x)))

sentiments <- as.data.frame(aperm(sentiments)) 
sentiments <- as.data.frame(lapply(sentiments, as.numeric))

emotions <- data.frame("count"=colSums(sentiments[,c(1:8)]))
emotions <- cbind("sentiment" = rownames(emotions), emotions)

ggplot(data = emotions, aes(x = sentiment, y = count)) +
  geom_bar(aes(fill = sentiment), stat = "identity") +
  xlab("Sentiment") + ylab("Total Count") + 
  scale_fill_brewer(palette='RdBu') + 
  theme_bw() + theme(legend.position='none')
```

```{r echo=FALSE}
wordsDataMatrix <- wordsDataFiltered%>% cast_tdm(word, Stock, n)

ap_td <- tidy(wordsDataMatrix)
ap_sentiments <- ap_td %>% inner_join(get_sentiments("bing"), by = c(term = "word"))

ap_sentiments %>%
  count(sentiment, term, wt = count) %>%
  mutate(n = ifelse(sentiment == "negative", -n, n)) %>%
  mutate(term = reorder(term, n)) %>%
  ggplot(aes(term, n, fill = sentiment)) +
  geom_bar(stat = "identity") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  ylab("Contribution to sentiment")

```


## AMZN = Amazon

```{r}
newsFile_AMZN  <- filter(newsFile_token_clean, Stock=='AMZN') %>% arrange(desc(tf_idf))
wordsFiltered <- newsFile_AMZN$word
wordsDataFiltered <- newsFile_AMZN[c('Stock','word','n')]
```

```{r echo=FALSE}
wordcloud(newsFile_AMZN$word,freq = newsFile_AAPL$n, colors=brewer.pal(8, "Dark2"), random.order=FALSE,scale=c(4,.01))
```

```{r echo=FALSE}
sentiments <- sapply(wordsFiltered, function(x) get_nrc_sentiment(as.character(x)))

sentiments <- as.data.frame(aperm(sentiments)) 
sentiments <- as.data.frame(lapply(sentiments, as.numeric))

emotions <- data.frame("count"=colSums(sentiments[,c(1:8)]))
emotions <- cbind("sentiment" = rownames(emotions), emotions)

ggplot(data = emotions, aes(x = sentiment, y = count)) +
  geom_bar(aes(fill = sentiment), stat = "identity") +
  xlab("Sentiment") + ylab("Total Count") + 
  scale_fill_brewer(palette='RdBu') + 
  theme_bw() + theme(legend.position='none')
```

```{r echo=FALSE}
wordsDataMatrix <- wordsDataFiltered%>% cast_tdm(word, Stock, n)

ap_td <- tidy(wordsDataMatrix)
ap_sentiments <- ap_td %>% inner_join(get_sentiments("bing"), by = c(term = "word"))

ap_sentiments %>%
  count(sentiment, term, wt = count) %>%
  mutate(n = ifelse(sentiment == "negative", -n, n)) %>%
  mutate(term = reorder(term, n)) %>%
  ggplot(aes(term, n, fill = sentiment)) +
  geom_bar(stat = "identity") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  ylab("Contribution to sentiment")

```

<br /> 

- Take 5 stocks to compare results with experts (websites) and explain [stockfluence](https://www.stockfluence.com/), [sentdex](http://sentdex.com/financial-analysis/), and [goldenrocks](http://goldenrocks.me/).





<br /> 

**Formato de entrega**: Mandar a los profesores y ayudante un **zip** con el nombre de "Tarea3_NumerodeCuenta" e incluir: **Rmd, HTML y los datos (texto en FORMATO TXT)**. Recuerden empezar a tiempo y utilizar piazza para colaborar entre todos con las dudas! Mucho éxito! 



