---
title: "R Packages and R Markdowns - Python and R"
author: "Maria Leonor y Ali Josue"
date: "September, 2018"
output: html_document
---

<br />


# Reticulate documentation

Pueden encontrar lo siguiente en la documentacion de "Reticulate": https://cran.r-project.org/web/packages/reticulate/vignettes/r_markdown.html


### R Markdown: Configuration

- library(reticulate)
- knitr::opts_chunk$set(echo = TRUE)
- use_python("/Users/AppData/Local/Continuum/Anaconda2/python")

```{r setup, include=FALSE}
#install.packages("stringi")
#install.packages("reticulate")
library(reticulate)
knitr::opts_chunk$set(echo = TRUE)
#use_python("/c/Users/MZAMORAMAASS/AppData/Local/Continuum/Anaconda2/python")
```



### Calling Python from R

"All objects created within Python chunks are available to R using the py object exported by the reticulate package. ""

```{python}
### Python
import pandas as pd
data = pd.DataFrame([1,2,3])
```

```{r}
### R
print (py$data)
```


### Calling R from Python

"You can analagously access R objects within Python chunks via the r object. "
"


```{r}
### R
data <- c('1','2','3')
```

```{python}
### Python
print r.data
```


<br />

# R Package: Quandl

Plataforma y paqueteria de datos llamada **Quandl**. En esta plataforma podr�s encontrar datos financieros, econ�micos y alternativos, �tiles para realizar an�lisis financiero cuantitativo. Para obtener acceso total a las bases de datos se necesita una cuenta premium; sin embargo, tambi�n es posible tener acceso gratuito a una muestra de la informaci�n por medio de la API. Para este ejercicio usaremos la API para obtener precios de acciones financieras y deber�s resolver lo que se te pide.

Primero que nada obten una cuenta gratuita con tu correo de estudiante en el website: https://www.quandl.com/

Esto te permitir� obtener un **API key**(una cadena de texto = string) que es necesaria para descargar informaci�n de la paqueter�a Quandl. Puedes conocer tu key accediendo a ACCOUNT SETTINGS > API KEY

Finalmente, instala el paquete [disponible en R: Quandl.pdf](https://cran.r-project.org/web/packages/Quandl/Quandl.pdf)

Ademas deberas instalar la paqueteria **Quantmod** y **TTR** que sirven para utilizar herramientas de Finanzas Cuantitativas.



<br />


# AAPL Prices and Returns

- Quandl Resource [here](https://docs.quandl.com/docs/r-time-series)
- Quandl = get stock prices and stock analysis functions
- Get the data: Quandl("EOD/AAPL", api_key=myKey, *start_date="2000-01-01"*)

```{r, fig.width = 2,echo=FALSE}
knitr::include_graphics('images/quandl.JPG')
```


```{r message=FALSE}
#install.packages("Quandl")
#install.packages("quantmod")
#install.packages("TTR")

library (Quandl)

#myKey <- 'xxxxxxxxxx'
#dataAAPL <- Quandl("EOD/AAPL", api_key=myKey, start_date="2000-01-01")
#write.csv(dataAAPL, file = "data/dataAAPL.csv")

dataAAPL <- read.csv("data/dataAAPL.csv")

knitr::kable( head(dataAAPL) )

```


```{r message=FALSE}
library(dplyr)

dataAAPL <- select(dataAAPL, c(Date, Close))

knitr::kable( head(dataAAPL) )

```


<br />


 - Convert into **"xts: Extensible Time Series"** format (you can type **??xts** in R studio to see more details).
 
 - XTS library = extensible time series 

 - Example: True time-based indexes. To allow for functions that make use of xts objects as a general time-series object - it was necessary to impose a simple rule on the class. The index of each xts object must be of a known and supported time or date class. At present this includes any one of the following - Date, POSIXct, chron, yearmon, yearqtr, or timeDate. The relative merits of each are left to the judgement of the user, though the first three are expected to be suficient for most applications.
 

```{r}
library(TTR)
library(quantmod)

#dataAAPL.xts <- xts(dataAAPL[,-1], order.by=dataAAPL[,1])
#dataAAPL.xts <- xts(dataAAPL[,-1], order.by=as.POSIXct(dataAAPL$Date))
dataAAPL.xts <- xts(dataAAPL[,-1], order.by=as.POSIXct(dataAAPL[,1]))

```


<br />

- You can compute periods -> days, months, quarters

```{r}
dataAAPL.months <- dataAAPL.xts [xts:::endof(dataAAPL.xts, "months")]

dataAAPL.months[1:10,]

```


<br />

- Quantmod package and TTR = compatible with xts
- Periodic **returns**

```{r}

dataAAPL.xts <- as.xts(dataAAPL.xts)
class(dataAAPL.xts)

dataAAPL.dailyReturns <- dailyReturn(dataAAPL.xts, subset=NULL, type='arithmetic')
dataAAPL.weeklyReturns <- weeklyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')
dataAAPL.monthlyReturns <- monthlyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')
dataAAPL.quarterlyReturns <- quarterlyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')

```

<br />

# Time Series tools 


<br />
- Querying and selections

```{r}
dataAAPL.monthlyReturns["2011"]

#  More options
#dataAAPL.monthlyReturns["20060101/20103112"]

```


<br />
- Transformation in time: Lags / backshift (backward) and Leads (forward) 

```{r}
# lag one step before with k = -1
before.dataAAPL <- lag(dataAAPL.xts, k = -1)
# lead one step after with k=1
after.dataAAPL <- lag(dataAAPL.xts, k = 1)

dataAAPL.lag <- cbind(dataAAPL.xts, before.dataAAPL)

head(dataAAPL.lag)
```


<br />
- Differences (stationary vs non-stationary data)

```{r}
dataAAPL.diff <- diff(dataAAPL.xts, differences = 2)

## Same as
#diff_by_hand <- dataAAPL.xts - lag(dataAAPL.xts, k=1)

head( dataAAPL.diff ) 
```


<br />
- Basic counts (days in a year !! convention)

```{r}
ndays( dataAAPL.dailyReturns["2011"] )
```


<br /> 

# Distributions and normality tests

```{r}
dataAAPL.quarterlyReturns[0:10,]
```

```{r}
plot(dataAAPL.quarterlyReturns, col="darkblue")
```

```{r}
summary(dataAAPL.quarterlyReturns)
```

```{r}
mean.quarterlyReturns <- mean(dataAAPL.quarterlyReturns)
sd.quarterlyReturns <- sd(dataAAPL.quarterlyReturns)
  
mean.quarterlyReturns 
sd.quarterlyReturns

hist(dataAAPL.quarterlyReturns,freq=FALSE, col="darkblue")
curve( dnorm(x,mean=mean.quarterlyReturns, sd=sd.quarterlyReturns), xlim=c(-1,2), col="red",lwd=3,add=TRUE)
```


<br />
- **Shapiro-Wilk** test of normality: p-value > 0.05 when data is not significantly different ("you cannot reject the hypothesis that the sample comes ...") from normal distribution
- Using "core" to get the returns only (GET RID OF THE INDEX)

```{r}
coreAAPL <- coredata(dataAAPL.quarterlyReturns$quarterly.returns)
shapiro.test(coreAAPL)
```


<br />

# R to Python dataset

- **R** core and Index separated

```{r}
coreAAPL <- coredata(dataAAPL.quarterlyReturns$quarterly.returns)
indexToR <- index(dataAAPL.quarterlyReturns)
```


- **Python**: read the variables (use ZIP to put columns together and DataFrame to set up the column properly)

```{python}
print type(r.coreAAPL), type(r.indexToR)

print r.indexToR[0:10]
print r.coreAAPL[0:10]

import pandas as pd
mycoreAAPL = [i[0] for i in r.coreAAPL]


dataAAPL_quarterlyReturns = pd.DataFrame( zip(r.indexToR, mycoreAAPL) , columns=['Date','Return'])

print dataAAPL_quarterlyReturns.head()
```


<br />

# Python: Dictionary

<br />
- We'll define variables with "_" instead of "." as in R

```{python}
print dataAAPL_quarterlyReturns.Date.dtype

dataAAPL_quarterlyReturns['Year'] = dataAAPL_quarterlyReturns.Date.dt.year

print dataAAPL_quarterlyReturns.head(10)
```

<br />
- **Examples and Demo**
- You can think about "SETS" to create unique values of the year 
- or.. you can also use "Numpy" library

```{python}

import numpy as np

print np.unique(dataAAPL_quarterlyReturns['Year'])

```


<br />
- We can subselect years in the dataframe and see the structure

```{python}

demoSelection = dataAAPL_quarterlyReturns[ dataAAPL_quarterlyReturns['Year']==2000]

print demoSelection 

```

<br />
- More than one dictionary: one for "list of prices"" and another one for "list of dates"
  dictPrices = {YEAR1: [price1, price2...]}  and  dictDates = {YEAR1: [date1, date2...]}
  
- or... One dictionary with different structures inside (nest) including dates AND prices -> demo here

```{python}


demo = list(zip(demoSelection.Date,demoSelection.Return))

#### STRUCTURE 1: TUPLES
for i in demo:
    print i, type(i)


def concatF(x):
    return str(x.Date)+ "||" + str(x.Return)

#### STRUCTURE 2: cONCATENATE
demo = demoSelection.apply(lambda x: concatF(x), axis=1)
print demo


def listF(x):
    return {'Date':x.Date, 'Return':x.Return}
    
#### STRUCTURE 3: DICTIONARY
demo = dict(zip(demoSelection.Date,demoSelection.Return))
print demo

```


<br />
- **Build the dictionary per year**

```{python}

dictReturns = {}
for year in np.unique(dataAAPL_quarterlyReturns['Year']):
    #Selection
    eachSelection = dataAAPL_quarterlyReturns[ dataAAPL_quarterlyReturns['Year']==year]
    #Include Selection in Dict
    dictReturns[year] = list(zip(eachSelection.Date,eachSelection.Return))
    
print dictReturns.keys()
```

- "List of tuples"

```{python}

print dictReturns[2000]

print dictReturns[2000][0]

```


<br /> 
 - Let's put that dictionary in a Dataframe
 
```{python}
dfReturns = pd.DataFrame.from_dict(dictReturns, orient='index').reset_index()
print dfReturns.head()

#### Split the tuples into price and return (diff columns)
for i in range(4):
    dfReturns[['Qtr'+str(i+1),'Return'+str(i+1)]] = dfReturns[i].apply(pd.Series)

#### Name sof columnas that we want
print [ i for i in dfReturns.columns if 'Qtr' in str(i) or 'Return' in str(i)]

```


<br /> 
- Use **MELT** to reshape then you can just do a LEFT JOIN

```{python}
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Qtr' in str(i)],
              var_name='Quarter', value_name='Date').tail()
              
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Return' in str(i)],
              var_name='ReturnQtr', value_name='Return').tail()
              
              
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Qtr' in str(i)],var_name='Quarter', value_name='Date').to_csv("data/AppleQtr.csv")

print dictReturns[2017]
```

<br /> 


# Similar to "Dictionary": R

<br /> 
- We can see how "R" transforms dictionaries

```{r}

rDict <- py$dictReturns
class(rDict)

### Keys in R
rDict["2013"]

### List names in R = KEYS
names(rDict)
```

<br /> 
- We can also create from scratch

```{r}
### New List
newDict <- vector(mode="list", length=length(rDict))

### Defines keys
names(newDict) <- names(rDict)
```


<br /> 
- Let's get all **1st quarters**

```{r}
### Dates
sapply( names(rDict),  function(x) rDict[x][[1]][[1]][[1]] )
### Returns
sapply( names(rDict),  function(x) rDict[x][[1]][[1]][[2]] )

```


<br /> 
- Let's get all **2nd quarters**....

```{r}
### Dates
sapply( names(rDict),  function(x) rDict[x][[1]][[2]][[1]] )
### Returns
sapply( names(rDict),  function(x) rDict[x][[1]][[2]][[2]] )

```



