---
title: "R Packages and R Markdowns - Python and R"
author: "Maria Leonor y Ali Josue"
date: "September, 2018"
output: html_document
---

<br />


# Reticulate documentation

Pueden encontrar lo siguiente en la documentacion de "Reticulate": https://cran.r-project.org/web/packages/reticulate/vignettes/r_markdown.html


### R Markdown: Configuration

- library(reticulate)
- knitr::opts_chunk$set(echo = TRUE)
- use_python("/Users/User/anaconda/bin/python")

```{r setup, include=FALSE}
#install.packages("stringi")
#install.packages("reticulate")
library(reticulate)
knitr::opts_chunk$set(echo = TRUE)
use_python("/Users/alimon/anaconda2/bin/python")
```



### Usando Python desde R

```{python}
### Python
import pandas as pd
data = pd.DataFrame([1,2,3])
```
Todos los objetos creados dentro de los chuncks Python {python} puedes llamarlos en R usando el objeto py exportado por el paquete reticulate.

```{r}
### R
print (py$data)
```


### Usando R desde python
Así mismo, tu puedes acceder a objetos de R dentro de chunks de Python  por medio de el objeto r


```{r}
### R
data <- c('1','2','3')
```

```{python}
### Python
print r.data
```


<br />

## I. Distribución de los Rendimientos

# R Package: Quandl

Plataforma y paquetería de datos llamada **Quandl**. En esta plataforma podr?s encontrar datos financieros, económicos y alternativos, ?tiles para realizar análisis financiero cuantitativo. Para obtener acceso total a las bases de datos se necesita una cuenta premium; sin embargo, también es posible tener acceso gratuito a una muestra de la información por medio de la API. Para este ejercicio usaremos la API para obtener precios de acciones financieras y deberías resolver lo que se te pide.

Primero que nada obten una cuenta gratuita con tu correo de estudiante en el website: https://www.quandl.com/

Esto te permitir? obtener un **API key**(una cadena de texto = string) que es necesaria para descargar informaci?n de la paquetería Quandl. Puedes conocer tu key accediendo a ACCOUNT SETTINGS > API KEY

Finalmente, instala el paquete [disponible en R: Quandl.pdf](https://cran.r-project.org/web/packages/Quandl/Quandl.pdf)

Ademas deberas instalar la paqueteria **Quantmod** y **TTR** que sirven para utilizar herramientas de Finanzas Cuantitativas.

<br />


# AAPL Prices and Returns

- Quandl Resource [here](https://docs.quandl.com/docs/r-time-series)
- Quandl = get stock prices and stock analysis functions
- Get the data: Quandl("EOD/AAPL", api_key=myKey, *start_date="2000-01-01"*)

```{r, fig.width = 2,echo=FALSE}
knitr::include_graphics('images/quandl.JPG')
```


```{r message=FALSE}
#install.packages("Quandl")
#install.packages("quantmod")
#install.packages("TTR")

library (Quandl)

#myKey <- 'xxxxxxxxxx'
#dataAAPL <- Quandl("EOD/AAPL", api_key=myKey, start_date="2000-01-01")
#write.csv(dataAAPL, file = "data/dataAAPL.csv")

dataAAPL <- read.csv("data/dataAAPL.csv")

### knitr::kable te permite imprimir las 
knitr::kable( head(dataAAPL) )

```


```{r message=FALSE}
library(dplyr)

dataAAPL <- select(dataAAPL, c(Date, Close))

knitr::kable( head(dataAAPL) )

```


<br />


 - Convertir en formato **"xts: Extensible Time Series"** (Puedes escribir **??xts** en R studio para obtener más detalles).
 
 - XTS library = extensible time series 

 -  True time-based indexes. Para permetir que las funciones hagan uso de objetos xts como un objeto de series de tiempo, fue necesario imponer una regla sencilla en la clase: El índice de cada objeto xts debe ser de una clase tiempo o fecha. (date or time class). Actualmente las clases de tiempo o fecha pueden ser cualquiera de las siguientes: Date, POSIXct, chron, yearmon, yearqtr, or timeDate.
 


```{r}
library(TTR)
library(quantmod)

#dataAAPL.xts <- xts(dataAAPL[,-1], order.by=dataAAPL[,1])
#dataAAPL.xts <- xts(dataAAPL[,-1], order.by=as.POSIXct(dataAAPL$Date))
dataAAPL.xts <- xts(dataAAPL[,-1], order.by= as.POSIXct(dataAAPL[,1]))

```


<br />

- Puedes calcular periodos -> days, months, quarters

```{r}
dataAAPL.months <- dataAAPL.xts [xts:::endof(dataAAPL.xts, "months")]

dataAAPL.months[1:10,]

```


<br />

- Quantmod package y TTR compatible con xts
- Periodic **returns**

```{r}

dataAAPL.xts <- as.xts(dataAAPL.xts)
class(dataAAPL.xts)

dataAAPL.dailyReturns <- dailyReturn(dataAAPL.xts, subset=NULL, type='arithmetic')
dataAAPL.weeklyReturns <- weeklyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')
dataAAPL.monthlyReturns <- monthlyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')
dataAAPL.quarterlyReturns <- quarterlyReturn(dataAAPL.xts,  subset=NULL, type='arithmetic')

```

<br />

# Herramientas de Series de Tiempo


<br />
- Consultas y selecciones

```{r}
dataAAPL.monthlyReturns["2011"]

#  More options
#dataAAPL.monthlyReturns["20060101/20103112"]

```


<br />
- Transformación en tiempo: Lags / backshift (backward) and Leads (forward) 

```{r}
# lag one step before with k = -1
before.dataAAPL <- lag(dataAAPL.xts, k = -1)
# lead one step after with k=1
after.dataAAPL <- lag(dataAAPL.xts, k = 1)

dataAAPL.lag <- cbind(dataAAPL.xts, before.dataAAPL)

head(dataAAPL.lag)
```


<br />
- Diferencias (stationary vs non-stationary data)

```{r}
dataAAPL.diff <- diff(dataAAPL.xts, differences = 2)

## Same as
#diff_by_hand <- dataAAPL.xts - lag(dataAAPL.xts, k=1)

head( dataAAPL.diff ) 
```


<br />
- Conteos Básicos (days in a year !! convention)

```{r}
ndays( dataAAPL.dailyReturns["2011"] )
```


<br /> 

# Distribuciones y Test de Normalidad

```{r}
dataAAPL.quarterlyReturns[0:10,]
```

```{r}
plot(dataAAPL.quarterlyReturns, col="darkblue")
```

```{r}
summary(dataAAPL.quarterlyReturns)
```

```{r}
mean.quarterlyReturns <- mean(dataAAPL.quarterlyReturns)
sd.quarterlyReturns <- sd(dataAAPL.quarterlyReturns)
  
mean.quarterlyReturns 
sd.quarterlyReturns

hist(dataAAPL.quarterlyReturns,freq=FALSE, col="darkblue")
curve( dnorm(x,mean=mean.quarterlyReturns, sd=sd.quarterlyReturns), xlim=c(-1,2), col="red",lwd=3,add=TRUE)
```


<br />
- **Shapiro-Wilk** test de normalidad: p-value > 0.05 cuando los datos no son significativamente diferentes de una distribución normal ("no puedes rechazar la hipotesis que se distribuyen normal ") 

- Usar "core" para obtener solamente los rendimientos (GET RID OF THE INDEX)

```{r}
coreAAPL <- coredata(dataAAPL.quarterlyReturns$quarterly.returns)
shapiro.test(coreAAPL)
```
El test nos dice que se rechaza la hipótesis que los rendimientos se distribuyen normal.

## Discusión: No-Normalidad en Rendimientos de Mercado
El supuesto que los renidimientos de activos estan normalmente distribuidos es ampliamente usado, implicitamente o explicitamente, en finanzas. Sin embargo, la evidencia en contra de este supuesto ha ido creciendo desde el articulo pionero de Mandelbort (1963), el cual argumentaba que los cambios en el precio podian ser caracterizados por una distribucion estable Pareto. Comunmente, la no-normalidad de los rendimientos ha sido evaluada empiricamente de la siguiente manera:

1) Colas Pesadas (Leptokurtosis): La primera forma de no-normalidad se relaciona con observar rendimientos extremos de mayor magnitud y mayor probabilidad que los implicitos en una distribución normal. En particular, la distribución normal no es adecuada para datos de frecuencias mayores tales como diaria o intradia. Las gráficas siguientes muestran la distribución de los rendiminetos del S&P 500 con frecuancias de 5 minutos y diaria. La linea azul (empirico) es mas alta en la punta y muestra mayor densidad en los extremos (i.e. leptokurtosis). 

```{r, fig.width = 1,echo=FALSE}
knitr::include_graphics('images/5minutes.jpg')
```

```{r, fig.width = 1,echo=FALSE}
knitr::include_graphics('images/daily.jpg')
```


<br />

# II. Crear estructuras de datos en R

# R a Python (dataset)

- **R** core e indice separados

```{r}
coreAAPL <- coredata(dataAAPL.quarterlyReturns$quarterly.returns)
indexToR <- index(dataAAPL.quarterlyReturns)
```


- **Python**: leer las variables (usa ZIP para poner la columnas junsas y DataFrame para establecer las columnas adecuadamente)


```{python}
### r.coreAAPL (use r. to access to R variables)

print type(r.coreAAPL), type(r.indexToR)

print r.indexToR[0:10]
print r.coreAAPL[0:10]

import pandas as pd
mycoreAAPL = [i[0] for i in r.coreAAPL]

### Define the dataframe using ZIP and r. variables
### We'll define variables with "_" instead of "." as in R


dataAAPL_quarterlyReturns = pd.DataFrame( zip(r.indexToR, mycoreAAPL) , columns=['Date','Return'])

print dataAAPL_quarterlyReturns.head()
```


<br />

# Python: Diccionarios

<br />

```{python}
print dataAAPL_quarterlyReturns.Date.dtype

dataAAPL_quarterlyReturns['Year'] = dataAAPL_quarterlyReturns.Date.dt.year

print dataAAPL_quarterlyReturns.head(10)
```

<br />
- **Ejemplos y Demos**
- Puedes pensar en "SETS" para crear valores unicos del año o tambien puedes usar "Numpy" library

```{python}

import numpy as np

print np.unique(dataAAPL_quarterlyReturns['Year'])

```


<br />
- Puedes seleccionar años en el dataframe y ver la estructura

```{python}

demoSelection = dataAAPL_quarterlyReturns[ dataAAPL_quarterlyReturns['Year']==2000]

print demoSelection 

```

<br />
- Más de un diccionaro: uno para "list of prices" y otro para "list of dates"

  dictPrices = {YEAR1: [price1, price2...]}  and  dictDates = {YEAR1: [date1, date2...]}
  
- o... un diccionario con diferentes estructuras adentro (nested) incluyendo fechas y precios -> demo here

```{python}


demo = list(zip(demoSelection.Date,demoSelection.Return))

#### STRUCTURE 1: TUPLES
for i in demo:
    print i, type(i)


def concatF(x):
    return str(x.Date)+ "||" + str(x.Return)

#### STRUCTURE 2: cONCATENATE
demo = demoSelection.apply(lambda x: concatF(x), axis=1)
print demo


def listF(x):
    return {'Date':x.Date, 'Return':x.Return}
    
#### STRUCTURE 3: DICTIONARY
demo = dict(zip(demoSelection.Date,demoSelection.Return))
print demo

```


<br />
- **Counstruir el diccionario por año**

```{python}

dictReturns = {}
for year in np.unique(dataAAPL_quarterlyReturns['Year']):
    #Selection
    eachSelection = dataAAPL_quarterlyReturns[ dataAAPL_quarterlyReturns['Year']==year]
    #Include Selection in Dict
    dictReturns[year] = list(zip(eachSelection.Date,eachSelection.Return))
    
print dictReturns.keys()
```

- "Lista de Tuples"

```{python}

print dictReturns[2000]

print dictReturns[2000][0]

```


<br /> 
 - Vamos a poner el diccionario en un Dataframe
 
```{python}
dfReturns = pd.DataFrame.from_dict(dictReturns, orient='index').reset_index()
print dfReturns.head()

#### Split the tuples into price and return (diff columns)
for i in range(4):
    dfReturns[['Qtr'+str(i+1),'Return'+str(i+1)]] = dfReturns[i].apply(pd.Series)

#### Name sof columnas that we want
print [ i for i in dfReturns.columns if 'Qtr' in str(i) or 'Return' in str(i)]

```


<br /> 
- Usa **MELT** para 

Use **MELT** para reorganizar (reshape), despues puedes usar un LEFT JOIN

```{python}
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Qtr' in str(i)],
              var_name='Quarter', value_name='Date').tail()
              
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Return' in str(i)],
              var_name='ReturnQtr', value_name='Return').tail()
              
              
print dfReturns.melt(value_vars= [ i for i in dfReturns.columns if 'Qtr' in str(i)],var_name='Quarter', value_name='Date').to_csv("data/AppleQtr.csv")

print dictReturns[2017]
```

<br /> 


# Similar a un "Diccionario": R

<br /> 
- Podemos ver como "R" transforma diccionarios

```{r}

rDict <- py$dictReturns
class(rDict)

### Keys in R
rDict["2013"]

### List names in R = KEYS
names(rDict)
```

<br /> 
- Lo podemos tambier crear desde cero

```{r}
### New List
newDict <- vector(mode="list", length=length(rDict))

### Defines keys
names(newDict) <- names(rDict)
```


<br /> 
-  Vamos a obtener **1st quarters**

```{r}
### Dates
sapply( names(rDict),  function(x) rDict[x][[1]][[1]][[1]] )
### Returns
sapply( names(rDict),  function(x) rDict[x][[1]][[1]][[2]] )

```


<br /> 
- Vamos a obtener **2nd quarters**....

```{r}
### Dates
sapply( names(rDict),  function(x) rDict[x][[1]][[2]][[1]] )
### Returns
sapply( names(rDict),  function(x) rDict[x][[1]][[2]][[2]] )

```



