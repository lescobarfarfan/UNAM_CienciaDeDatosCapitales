---
title: "Mercado de Capitales y Estrategias de Inversión"
author: "Maria Leonor y Ali Josue"
date: "Nov 13, 2018"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<br />

# 1. Motivación

<br />

Una vez que hemos puesto en práctica los modelos de aprendizaje automático en distintos escenarios del Mercado de Capitales, es muy importante mantener una idea clara de cómo usar las herramientas de Ciencia de Datos para proponer, evaluar y comparar inversiones. Para ello, vamos a hablar de los principios básicos de construcción de portafolios, métricas de riesgo y herramientas de visualización para comparar con el mercado. Lo anterior, desde el punto de vista riesgo vs rendimiento.

<br />

# 2. Teoría de Riesgo y Retorno

<br />

Para hablar de construcción de portafolios, iniciamos introduciendo la [tarea de regresión lineal y CAPM](https://nbviewer.jupyter.org/github/UNAMCiencias-Seminario-CienciaDeDatos/Seminario_Ciencias/blob/master/Tarea/04_Tarea_Regresion_CAPM/hw_4_Regresion_CAPM.html), donde nos referimos a los precios de los instrumentos en el mercado y su equilibrio con respecto a la oferta y demanda. En esta tarea mencionamos la base de este equilibrio, conocida como **Modern Portfolio Theory (Harry Markowitz - _1952 Portfolio Selección_, Ph.D. dissertation University of Chicago - Nobel Prize in Economics 1990)**, cuyo objetivo consiste en **maximizar retornos dado un nivel de volatilidad, definido como la desviación estándar de los retornos**. También, hablamos de un modelo de regresión lineal en específico, Capital Asset Pricing Model (CAPM), que surge a partir de la teoría moderna e introduce el concepto de **alpha**, el **retorno en exceso** (retorno de un instrumento vs el mercado), y **beta como el riesgo** relativo al mercado. Desde entonces, herramientas como **multifactor models** que mencionamos en la tarea y, en general, las estrategias para distribuir y colocar activos financieros, han revolucionado el análisis de los aspectos determinantes para evaluar el _**performance (en riesgo vs rendimiento)**_ de un portafolio. 

<br />

En otras palabras, se dice que los precios incorporan **toda la información disponible en el mercado**, por lo cual podemos usar dicha información para obtener la mínima varianza o retornos máximos. Se establece que lo que esta en manos del inversionista es elegir su grado preferencia _riesgo vs rendimiento_ en una combinación de activos (_**portafolio eficiente**_) cuyo punto óptimo corresponde a la solución de un problema de programación no lineal donde esperamos un trade-off de la siguiente forma[^1]:

a) Varianza mínima en todas las combinaciones, con un rendimiento esperado definido
b) Rendimiento esperado máximo en todas las combinaciones posibles, dada una varianza

Como veremos en el siguiente punto de estas notas, se trata de un método de _n variables_ y _k restricciones_, que es convertido a uno sin restricciones con _n + k variables_ para facilitar su solución. 

<br />

```{r out.width = "70%", echo=FALSE}
myimages<-list.files("images/", pattern = "EfficientFrontier.jpg", full.names = TRUE)
knitr::include_graphics(myimages)
```

<span><small><i>"Efficient frontier, represented by the curved solid line in Figure 5-1. Put formally, this efficient frontier contains all portfolios of assets such that there are no other portfolios (or assets) that for a given amount of risk (in terms of standard deviation of rates of return) offer a higher expected rate of return." THE ESSENTIALS OF
RISK MANAGEMENT: Michell Crouhy, Dan Galai, Robert Mark - McGraw-Hill</i></small></span>

<br /> 


Es aquí donde entonces pensamos en los fundamentos para analizar una inversión, tratando de estar alineados con este concepto de un portafolio eficiente.

<br />

**2.1 Objetivos de la inversión**

<br />


En el tema de [Business Understanding de Ciencia de Datos](https://nbviewer.jupyter.org/github/UNAMCiencias-Seminario-CienciaDeDatos/Seminario_Ciencias/blob/master/RNotebooks/11_Intro_Business%20Understanding/11_Intro_Business_Understanding.html) estuvimos hablando de distintos tipos de inversiones con base en los activos financieros **(i.e. savings accounts, direct investing & indirect invesment)**. En este caso, en el tema de Mercado de Capitales en el que nos estamos enfocando (a diferencia del ahorro donde no pondríamos el capital en riesgo), corresponde a inversiones directas **(_fixed income & equity_)**, donde la selección de instrumentos depende de los siguientes parámetros:

1. Análisis del flujo de efectivo
2. Niveles de riesgo: Apetito o aversión (i.e. agresivos, moderados y conservadores) 
3. Horizontes de tiempo: Corto, Mediano o Largo Plazo
4. Capacidad de inversión y limitantes: Geografía, regulación, tipo de operación, etc.
5. Necesidad de cubrir pasivos

<br />

**2.2 Implementación de la inversión**

<br />


Para poder implementar la inversión y hacer la distribución de los activos (_Asset Allocation_) necesitamos de dos pasos básicos: 

1. Definir una estrategia de colocación de activos (e.g. pasiva o activa), buscando el _performance_ optimo
2. Definir la estructura de instrumentos, construir el portafolio y un benchmark 

Entre otros aspectos, se buscará **diversificar** las estrategias con base en el riesgo, el tipo de instrumentos y de compañías:

- Value & Growth (high vs low book-to-market)
- Large, Mid & Small Cap (size effect)
- Domestic & International
- Stocks, Bonds & Cash
- Markets, Sectors & Industries 

<br />

**2.3 Revisión, evaluación y ajuste**

<br />


Como parte fundamental de una inversión, debemos estar siempre al tanto de que los objetivos se estén cumpliendo. Para ello, será necesario **monitorear continuamente las exposiciones y los riesgos**, para realizar ajustes y adaptarse a las condiciones del mercado cada que sea necesario. En otras palabras, se convierte en un proceso iterativo en función de las operaciones, la regulación y los inversionistas involucrados. 

Durante este proceso nos vamos a encontrar con la posibilidad de analizar los componentes de un portafolio con base en los siguientes elementos: 

* Weights: Holding and Sectors
* Volume, Dividends and splits
* Turnover rate (portfolio's churn: Buy&Hold vs HighReplacement)
* Ratios: Price to Earnings, Price to Sales, Price to Book, etc.

Y podemos hacer también una evaluación desde el punto de vista técnico / estadístico: 

* Standard Deviation
* Alpha & Beta
* Correlation
* R-Squared
* Upside & Downside Capture Ratios ([_video ref_](https://www.youtube.com/watch?v=GakNEJM4uvM))

<br />


# 3. Portafolios de inversión

<br />

Tenemos un portafolio compuesto por $n$ activos, donde para calcular la rentabilidad y el riesgo usamos un promedio de los rendimientos $r_i$ ponderados por los pesos $w_i$ de sus componentes.

<br /> 

Por un lado, asumiendo que la inversión tiene una relación lineal y bajo la misma notación de vectores/matrices que en los modelos anteriores, tendremos lo siguiente:


**Rendimiento total** = $\; R_p \;  = \; \sum_{i=1}^n w_i r_i \;  = \; W^T R$


<center> **Rendimiento total esperado**  </center> 

$$\; \mu_p \; = \; E[R_p] \; = \; \sum_{i=1}^n \;w_i \;E[r_i]$$

Donde $W^T$ es el vector de pesos en todo el portafolio, por lo cual está condicionado a que la suma total sea igual a 1.. a la ponderación total del portafolio $\sum_{i=1}^n w_i = 1$.

<br />

Por otro lado, la varianza del rendimiento del portafolio nos ayuda a medir el riesgo y para ello, necesitamos conocer las varianzas y covarianzas del portafolio.


**Varianzas** = $\sigma_{ii} = var[r_i] = E [ \; (r_i - E[r_i])^2 \;]$ 

**Covarianzas** = $\sigma_{ij} = var[r_i,r_j] = E [ \; (r_i - E[r_i]) \; (r_j - E[r_j]) \;]$

<center>  **Varianza total del portafolio**  </center> 

$$ \sigma_{p} \; = \; var[R_p] \; = \; \sum_{i=1}^n \sum_{j=1}^n \;w_iw_j \; \sigma_{ij} \; = \; w^t \; \Sigma \; w$$ 



Los **supuestos más importantes** para el planteamiento de este problema son: La matriz $\Sigma$ y el vector de Rendimientos esperados por activo se asumen como variables conocidas[^2]. Cada inversionista bajo este esquema puede comprar cualquier monto de los activos, positivo o negativo. Además, bajo este planteamiento el precio es independiente del monto comprado (_no moving market_) y NO hay costos de transacción.

Una vez definido el problema, de manera más formal vamos a poder comprender mejor el problema de optimización (**multiplicadores de La grange**). Similar a la búsqueda de _menor perdida_ en los problemas de Machine Learning, en este caso buscamos que un portafolio sea eficiente, minimizando la varianza $\sigma_{p}$, dado un rendimiento esperado $\mu_p$ y la suma de los pesos totales igual 1. 


<br />

# 4. Métricas de riesgo

<br />

Otro punto muy importante para la clase consiste en entender las **métricas de riesgo básicas** que debemos monitorear, principalmente para (1) comparar las estrategias con el mercado y (2) calcular las pérdidas potenciales a las que nos enfrentaríamos con estos portafolios.

En ese sentido existen distintos tipos de riesgo para evaluar:

1. Riesgo de Mercado: Cambio en el valor de instrumentos y, por ende, en el valor de los portafolios. Esto resulta como parte de movimientos en el mercado, por ejemplo, en tasas de interés, tipos de cambio, precios, etc..

2. Riesgo de Crédito: Incumplimiento por parte de un deudor o contraparte.

3. Riesgo de Liquidez: Incapacidad para cumplir en tiempo y forma con flujos de efectivo esperados o inesperados.

4. [Otros riesgos](https://www.cmra.com/risk-advisory/): Riesgo Operativo, Riesgo Legal, Riesgo País, Riesgo Reputacional, Riesgo Sistémico, ...


<br />

# Referencias

- _Modern Portfolio Theory and Investment Analysis by Edwin J. Elton, Martin J. Gruber, Stephen J. Brown, William N. Goetzmann_

- _A Random Walk Down Wall Street: The Time-Tested Strategy for Successful Investing, by Burton G. Malkiel_

- _When Genius Failed: The Rise and Fall of Long-Term Capital Management, Random House by Roger Lowenstein_

- _The Four Pillars of Investing: Lessons for Building a Winning Portfolio, by William J. Bernstein. McGraw Hill_

- _The Essentials for Risk Management, by Michell Crouhy, Dan Galai, Robert Mark - Second Edition - 2014 McGraw-Hill_


[^1]: "..in the context of mean/variance analysis, a portfolio is called inefficient if it is possible to increase the expected return without increasing the variance, or (which is the same thing), if it is possible to decrease the variance without decreasing the expected return. A portfolio is efficient if it is not inefficient." _NYU Courant Institute of Mathematical Sciences - Jonathan Goodman - Risk and Portfolio Management with Econometrics_

[^2]: Si se tiene un valor negativo en la matriz de varianzas, normalmente se asume como una _venta en corto = shorting_ (i.e. vender un activo financiero sin tenerlo, para después pagar el diferencial o adquirirlo en el mercado. Si se tiene un valor negativo de la tasa libre de riesgo estamos hablando de un préstamo (_borrowing_)] 








