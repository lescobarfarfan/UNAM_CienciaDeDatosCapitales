---
title: 'Decision Trees: Regression & Classification'
output:
  html_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

<style>
body {
text-align: justify}
</style>



# Introducción.

<br />

Los árboles de decisión constituyen  un enfoque no-paramétrico en cuanto a lo que se refiere a regresión. En general, los métodos basados en árboles hacen una partición del espacio de características en una serie de rectángulos en donde en cada rectángulo, los puntos contenidos ahí tienen características similares; una vez dividido el espacio, se ajusta un modelo simple en cada rectángulo.  La forma en como el grueso de los algorítmos particionan el espacio es mediante **Particiones Recursivas Binarias**, aunque también existe la posibilidad de realizar particiones múltiples.

<br />

La finalidad es encontrar el árbol de deciciones óptimo minimizando el error, aunque cabe mencionar que pueden existir otras métricas, como minimizar el número de nodos, por ejemplo. Se ha mostrado que el problema de encontrar el árbol mínimo consistente con el conjunto de entrenamiento es NP-complejo, lo cual indica que usar árboles de decisión podría ser factible solo con conjuntos de datos relativamente pequeños. Dada esta problematica, es necesario implementar métodos heurísticos para resolver el problema, y dentro de éstas existen dos grupos, los denominados $top-down$ y los $bottom-up$.  

<br />

Por su simpleza, los $bottom-up$ han ganado popularidad, y dentro de esta categoría existe el $approach$ ID#, C4.5, CART, en donde, tanto CART como C4.5 se enfocan en crecer el árbol y podarlo, en tanto ID3 se enfoca solo en la fase de crecimiento.

<br />

El enfoque al que pondremos atención es al denominado *CART*, que es el acrónimo para $\textit{Classification and Regression Trees}$. Como su nombre indican, clasificación se refiere a dividir un conjunto de elementos en grupos donde cada grupo comparta características, en tanto en regresión, el objetivo es hacer alguna predicción de la variable independiente, como el salario, por ejemplo.

<br />

El espacio de características (feature space) se divide en rectángulos de tal forma que las observaciones con valores de respuesta similar estén agrupados. Después de que la partición se termina, las variables respuesta tienen un valor constante dentro de cada área. Para comenzar, asumiremos que tenemos que realizar una regresión basada en árboles, asumiremos también que nuestro problema es relativamente simple, de tal forma que hay solo dos predictores $X_{1}$ y $X_{2}$ con variable respuesta contínua $Y$ en el $[0,1]$.


Comencemos con las particiones verticales. Aquí podríamos designar nuestra partición inicialmente de forma arbitraria, pero con el riesgo de que tal vez, nuesto primer intento de particionar el espacio de características resulte en un espacio mal particionado como en la siguiente figura. 

<br />

<center>
![Partición con separador a la izquierda.](images/clusterLeft.png){width=400px}
</center>

<br />

Dado lo anterior, necesitamos algún criterio que nos indique en donde poner el separador de tal forma que este criterio reduzca la posibilidad de clasificar puntos de forma equivocada. Por otro lado, aun si realizamos esta primera partición vertical de forma correcta, nuestro conjunto de datos podría ser mas complicado, de tal forma que, necesitaríamos realizar particiones horizontales, como en la siguiente figura.

<br />

<center>
![Particiones Múltiples.](images/clusterAll.png){width=400px}
</center>

<br />

Ahora, antes de entrar en mayor detalle en cuanto al criterio de clasificación (partición) del espacio de características, es menester mencionar que si el espacio esta bien particionado como en la figura de arriba, entonces podríamos tomar cualquier rectángulo, digamos el superior izquierdo, podríamos tomar un subconjunto de puntos, seleccionar una variable y calcular el promedio. Si ahora seleccionamos otro subconjunto de ese mismo rectángulo y volvemos a calcular el promedio, tedríamos que estas cantidades deberían de ser esencialmente las mismas, siempre y cuando la clusterización esté bien realizada. 

<br />

Mas aún, el proceso de separación se puede llevar a cabo en mas dimensiones, y la creación de cada subdivisión, puede ser hecha mediante un árbol, como se muestra en la siguiente figura. El proceso en específico se mostrará mas adelante.


<center>
![Particiones Múltiples.](images/moreDimTree.png)
</center>

<br />

Ahora veámos con mayor formalidad como operan éstos algorítmos.


# Criterio de Separación.

Por lo general, como se dijo, los criterios de separación involucran una sola variable, pero existe la posibilidad de que sean multivariados. Los criterios pueden ser caracterizados de acuerdo a:

- el orígen de la medida: teoría de la información, distancia, dependencia.
- la estructura de la medida: medidas basadas en la impureza, criterios binarios.

De particular interés será la medida basada en **impureza** por su popularidad como criterio de partición.

<br />

Dada una variable aleatoria $x$ con $k$ valores discretos distribuidos de acuerdo a $P=(p_{1},p_{2},...,p_{k})$, se define una medida de **impureza** como una función $\phi=[0,1]^{k}\rightarrow \mathbb{R}$ que satisface las siguientes condiciones. 

- $\phi(P)\geq0$
- $\phi(P)$ es mínimo si existe $i$ tal que tal que $p_{i}=1$
- $\phi(P)$ es máximo si $\forall i, 1\leq i \leq k$, $p_{i}=1/k$
- $\phi(P)$ es diferenciable en todao su dominio.

En regresión basada en árboles, es común usar el Error Cuadrático Medio como medidad de **impureza**, aunque existen otras medidas, como **information gain** y el **índice de Gini**. Mas formalmente, una medida de impureza se define de la siguiente forma:

<!-- <br />

<center>
![](images/impurity.png) </center>

<br />-->


Por otro lado,  cada vez que se realiza una partición del espacio de características, se puede llevar un registro de cómo ocurrieron estos cortes, y el registro toma forma de un árbol. A continuación se presenta el pseudo-algoritmo para lograrlo.

<br />

**Creación del Árbol.**

Sea $(S,A,y)$ donde:

- $S$ = conjunto de entrenamiento.
- $A$ = Conjunto de características.
- $y$ = Característica objetivo (variable dependiente).

<br />

Se debe de tener una función $f(A)$ de los atributos (insumos del modelo) tal que el partir $S$ deacuerdo con los resultados de $f(A)$, $(X_{1},X_{2},,...,X_{n})$, obtenga el mejor resultado de separación de acuerdo a una métrica.

<br />

- Crear un árbol con un solo nodo.
- **IF** la métrica de separación es mayor umbral, agrega la etiqueta $f(A)$ a ese nodo $t$. 
-   **FOR** $\forall v_{i} \epsilon f(A)$ Asigna conexión desde el nodo raíz al $Subárbol_{i}$ con una rama etiquetada $X_{i}$

<br />


# Árboles de Regresión.

<br />

Parte de la idea central tanto en regresión como en clasificación es primero separar el espacio de características y porsteriormente aplicar un modelo simple en cada una de las subdivisiones creadas. Se comenzará presentando el cómo se construye el árbol mediante un ejemplo simple y posteriormente se presentará las medidas de $impureza$ mediante las cuales el algoritmo realiza los $splits$. 

<br />

Para hacer una predicción para una observación dada, el método típicamente utiliza la media del conjunto de entrenamiento de la región a la que pertenece. Las regiones podrían tener formas muy variadas, pero éste método elige rectángulos (o bien, rectángulos en grandes dimensiones). Éste método busca encontrar $cajas$ denomindas como $R_{1},R_{2},...,R_{m}$ que minimice una medida de **impureza**, esta medida es la suma de residuales al cuadrado: 

$$RSS=\sum_{j=1}^n \sum_{i\epsilon R_{j} } (y_{i}- \hat{y}R_{j} )2$$

<br />

donde:

$\hat{y}R_{j}=$ respuesta promedio de las observaciones de entrenamiento.


A menudo no es factible calcular toda posible partición, así que se utiliza un algoritmo $greedy$ $top-down$ que es el de particiones binarias recursivas. Se le llama $top-down$ porque comienza en la parte de arriba del árbol, y luego sucesivamente parte el espacio. 

<br />

Otra estrategia es crear un árbol grande y despues **podarlo** (eliminar nodos) para obtener un sub-árbol que lleve a la tasa de error de prueba mas baja. Dado un subárbol, se puede estimar el error de prueba usando $k-fold$ $cross-validation$, aunque de hecho, ésto también es ineficiente, pues a menudo es muy costoso realizar $cross'validation$ para cada subárbol. Un método alternativo denominado, liga mas débil es la opción mas utilizada y se presentará mas adelante. 

<br />


Para comenzar, imaginemos que el algoritmo decide hacer un corte $X_{1}=20$. En términos del árbol, esta partición genera dos regiones, aquella donde sus elementos cumplen con que $X_{1}<20$ y otra donde $X_{1}>20$, lo cual se muestra en el árbol del lado derecho.  

<br />

<center>
![](images/9.png){width=400px} ![](images/10.png){width=400px}
</center>

<br />

En el siguiente paso, el algoritmo decide hacer un corte horizontal para los valores donde $X_{1}>20$ pero $X_{2}=170$. Entonces, en el árbol debemos tomar la rama de **no** en el nodo $X_{1}<20$ y crear otro nodo con etiqueta $X_{2}<170$.


<br />

<center>
![](images/11.png){width=400px} ![](images/12.png){width=400px}
</center>

<br />

El algoritmo decide ahora realizar el **Split 3** de forma horizontal en $X_{2}=200$ pero solo para los valores donde $X_{1}<20$. Por ende, se debe de tomar la rama de **si** en el nodo $X_{1}<20$ y crear un nuevo nodo con etiqueta $X_{1}<200$.

<br />

<center>
![](images/13.png){width=400px} ![](images/14.png){width=400px}
</center>

<br />

Finalmente el algoritmo decide hacer un último corte en $X_{1}=40$ pero con $X_{2}<170$. Así, en el árbol tenemos que adicionar un nodo con etiqueta $X_{1}<40$ con nodo antecesor $X_{2}<170$.

<br />

<center>
![](images/15.png){width=400px} ![](images/16.png){width=400px}
</center>

<br />

Finalmente, cómo esta partición nos ayuda a predecir el valor de la variable dependiente $Y$? Toma los promedios de cada región (ramas terminales) y ese es el valor que se va a asignar a cualquqier nuevo punto que caiga en la respectiva región. 

<br />

<center>
![](images/17.png){width=400px} ![](images/18.png){width=400px}
</center>

<br />

Mas formalmente, nuestro dataset consistirá de $p$ insumos, una variable respuesta y N observaciones, $(x_{i},x_{i})$ donde cada $(x_{i}=(x_{i1},x_{i2},...,x_{ip})$. Si inicialmente existen M regiones $R_{1},R_{2},...,R_{M}$ y se modela la respuesta como una constante en cada región:

$$f(x)=\sum_{m=1}^nc_{m}I(x \epsilon R_{m})$$

Se busca minimizar la suma de cuadrados, de tal forma que el mejor estimador $\hat{c_{m}}$ es el promedio en cada región 

$$\hat{c_{m}}=ave(y_{i}/x_{i} \epsilon R_{m})$$

Ahora bien, se verá que criterio utilizar para realizar la partición. Considera la variable de partición $j$ (es decir, fija una de las variables ya sea el éje $x$ o el $y$) y sea $s$ el punto de partición de tal forma que se definen dos medios planos, antes y después del punto $s$.

$$R_{1}(j,s)=\{X/X_{j}\leq s\} $$
$$R_{2}(j,s)=\{X/X_{j}\geq s\} $$

Dadas éstas regiones separadas por $s$ se busca el punto que resuelva el siguiente probema de optimización:

$$min_{j,s}[min_{c_{1}} \sum_{x_{i} \epsilon R_{1}(j,s)} (y_{i}-c_{1})^2   +   min_{c_{2}} \sum_{x_{i} \epsilon R_{2}(j,s)} (y_{i}-c_{2})^2 ]$$

Dada la elección de $j$ se puede recorrrer el punto de separación para encontrar el punto que haga la minimización de la parte de afuera. Por otro lado, los dos elementos que minimizan la parte interna son los siguientes:


$$\hat{c_{1}}=ave(y_{i}/x_{i} \epsilon R_{1})$$

$$\hat{c_{2}}=ave(y_{i}/x_{i} \epsilon R_{2})$$

Nótese que si se incluyen muchas separaciones podemos incurrir en un problema de $overfitting$ mediante el cual, si llega una nueva observación, será muy dificil que quede correctamente clasificada en alguna de las separaciones existentes. Una forma de decidir cuántas particiones incluir es mediante un número predeterminado.  

<br />

Una vez teniendo el árbolo, el siguiente paso es podarlo ($prunning$), lo cual se refiere a obtener cualquier sub-árbol T del árbol original ($T_{0}$). Mas formalmente, un subárbol T de $\supset T_{0}$  como cualquier árbol que se obtenga a través de podar $\supset T_{0}$, es decir, el que se obtiene eliminando un número de sus nodos terminales los cuales son indexados por $m$ y donde ese nodo representa la región $R_{m}$. Sea $|T|$ el cardinal de nodos terminales en T y sean,

$$N_{m}= \# (x_{i} \epsilon R_{m} )$$
$$\hat{c}_{m} = \sum_{x_{i} \epsilon R_{m}} (y_{i}-\hat{c}_{m})^2$$
 De esa forma, se define el criterio de costo-complejidad de a siguiente forma. En vez de considerar todos los subárboles posibles, considera una susesión de árboles indizados por un parámetro $\alpha$ de ajuste ($tuning$), el algoritmo es el siguiente:
 
1. Use particiones recursivas binarias para crear un árbol grande con los datos de entrenamiento, se detiene solamente cuando el nodo terminal tiene menos de un número predeterminado de observaciones.
2. aplique podado costo-complejidad al árbol grande con la finalidad de obtener subárboles, como función de $\alpha$
3. Usar $K-fold$ $cross-validation$ para seleccionar $\alpha$, es decir, dividir el conjunto de entrenamiento en K pedazos y para cada uno de esos pedazos.
  + Repita los pasos 1 y 2 en todos menos en el pedazo K de los datos de entrenamiento.
  + Evalúe el erro cuadrático medio de predicción en el K pedazo que no se utilizó, como función de $\alpha$.
  + Calcula el promedio de los resultados para cada $\alpha$, y selecciona el $\alpha$ que minimiza el error promedio.
4. Regresa el subárbol  del paso 2 que corresponde a ese valor de $\alpha$.
 
 
 El error cuadrático medio se calcula de la siguiente forma, esta es la cantidad que se debe minimizar:
 
 
 $$RSS=\sum_{m=1}^{\mid T \mid} \sum_{x_{i}\epsilon R_{j} } (y_{i}- \hat{y}R_{m} )^2+ \alpha\mid T \mid$$

 Donde:
 
 - $\mid T \mid=$ indica el número de nodos terminalesdel árbol T.
 - $R_{m}$ es el rectángulo o caja correspondiente al m-ésimo nodo terminal.
 - $\hat{y}R_{m}$ es la respuesta predicha asociada a $R_{m}$.
 - $\alpha$ controla el intercambio entre la complejidad del árbol y si ajuste a los datos de entrenamiento, valores grandes de $\alpha$ resultan en árboles chicos.
 - Para cada $\alpha$ existe un subárbol para el cual $RSS$ es tan mínimo.
 

# Aplicación de Árboles de Regresión.


De que trata la base de datos?

Es una base de datos de una empresa anónima, solo se tienen dos variables años de experiencia total y salario. Así que la idea es tratar de revisar la existencia de alguna relación entre estas dos variables. Adicionalmente, si una persona perdió su empleo y llega a esta empresa a pedir empleo, ésta base de datos debería de ser suficiente para predecir su sueldo dada la experiencia laboral de la persona. Como es una entrevista laboral, las personas tienden a mentir tanto de sus capacidades como de su sualdo anterior, así que este modelo podría servir para saber si la persona esta diciendo la verdad o esta alardeando (**truth or bluff**). Empleado dijo que gana USD$160,000 al año.

<br />

Notablemente, este ejemplo es muy simple, no toma en cuenta otras variables como la carreca que estudió, la universidad donde la estudió, si tiene educación adicional etc. Sin embargo, es una base con la información necesaria para explicar el punto a tratar. 


<br />

Cargar la base de datos 

```{R}
dataset = read.csv('data/Position_Salaries.csv')
dataset = dataset[2:3]
head(dataset)

```

Recordemos que la persona para quien estamos tratando de predecir su salario dijo que éste había sido de USD\$160,000. Veámos que predice nuestro modelo.

```{R}
library(rpart)

regressor = rpart(formula = Salary ~ .,data = dataset, control = rpart.control(minsplit=1))

y_pred = predict(regressor, data.frame(Level=6.5))
y_pred
```
El salario predicho del modelo es mucho mas alto que lo que la persona mencionó. Esto podría significar un arror de predicción muy grande, pero primero hay que revisar la gráfica para tener una mejor idea de lo que ocurre.



```{R}
library(ggplot2)
ggplot() +
  geom_point(aes(x = dataset$Level, y = dataset$Salary),
             colour = 'red') +
  geom_line(aes(x = dataset$Level, y = predict(regressor, newdata = dataset)),
            colour = 'blue') +
  ggtitle('Truth or Bluff (Regression Model)') +
  xlab('Level') +
  ylab('Salary')
```

En el gráfico anterior hay un problema. No debería haber líneas diagonales, dado que el algorítmo solo hace cortes horizontales y verticales, lo cual quiere decir que hay algo que no estamos haciendo bien. Por otro lado, recordemos que este tipo de modelos son no lineales y no necesariamente contínuos, de donde al revisar la escala de la gráfica, un éje esta en miles, y el otro no, así que puede ser que el modelo este bien hecho y el problema radica en la escala. La siguiente gráfica corrige la escala para mostrar un gráfico mas adecuado.

```{R}
library(ggplot2)
x_grid = seq(min(dataset$Level), max(dataset$Level), 0.1)
ggplot() +
  geom_point(aes(x = dataset$Level, y = dataset$Salary),
             colour = 'red') +
  geom_line(aes(x = x_grid, y = predict(regressor, newdata = data.frame(Level = x_grid))),
            colour = 'blue') +
  ggtitle('Truth or Bluff (Regression Model)') +
  xlab('Level') +
  ylab('Salary')
```

Recordemos que el modelo toma el promedio por cada partición horizontal, y eso es lo que esta mostrando.


Credit: Super Data Science.




















#  Árboles de Casificación.

Objetivo: Maximizar el número de elementos de una categoría en particular en una región.

<br />

El algoritmo es muy similar, lo que cambia es la forma en como se seleccionan los nodos y se poda el árbol. En regresión se uso el Error Cuadrático Medio, como medidad de $\textit{impureza}$. En clasificación existen otras mas basadas en la proporción de elementos que existen en una región determinada. Para el nodo $m$, el cual representa la región $R_m$ con $N_m$ observaciones se define la proporción de observaciones en la clase $k$ de la siguiente forma:

$$\hat{p}_{mk}=(\sum_{x_{i} \epsilon R_{m}}I(y_{i}=k))/N_{m}$$
De esa forma, se clasifican las observaciones del nodo $m$ en la clase $k(m)=argmax_{k}\hat{p}_{mk}$. 

<br />
<!--
<center>
![](images/impurity.png) </center>

<br />
--> 
Existen otras medidas de impureza, entre ellas:

**Índice de Gini:**

- Es un índice que favorece particiones grandes.
- Grupos perfectamente clasificados tienen un índice $=0$, i.e. se deben buscar Ginis tan pequeños como sea posible.


$$\sum_{k \ne  k'} \hat{p}_{mk} \hat{p}_{mk'} = \sum_{k=1}^K \hat{p}_{mk} (1-\hat{p}_{mk})$$

Ejemplo:


<!-- <br />

<center>
![](images/gini.png) </center>

<br /> -->

$$Gini=1-(3/8)^2-(5/8)^2=15/32$$

Si se realiza la partición en $x_{1}<3.5, \bigtriangleup Gini=15/32-(3/8)*0-(5/8)*0=15/32$ 

Si se realiza la partición en $x_{1}<4.5, \bigtriangleup Gini=15/32-(4/8)*(3/8)-(4/8)*0=9/32$ 



**Entropía**

Mientras mas alta la entropía, mas contenido de información, es, decir, se busca una entropía baja.

$$-\sum_{k =  1}^K \hat{p}_{mk}log(\hat{p}_{mk})$$

<!-- <br />

<center>
![](images/entropy.png) </center>

<br /> -->


- La entropía de un grupo donde todos los elementos pertenecen a la misma clase, es cero.
- La entropía máxima se alcanza cuando la mitad de los elementos en un conjunto pertenecen a un grupo, y la otra mitad pertenece a otro.


Estas medidas suelen parecerse, sin embargo, la entropía y el índice de Gini son diferenciables, lo cual es una característica importante en análisis numérico. 

<br />


**Information Gain**

Con las medidas anteriores, se pretende determinar los atributos mas relevantes para discriminar entre clases. **Information Gain se puede pensar como la tasa de cambio o la velocidad de cambio de la entropía**. Si tenemos una variable $X_k$, podemos evaluar la ganancia lograda con un split en otra variable (por ejemplo, $X_{k+1}$). Todo esto, a traves de la diferenia de entropias por variable / split.

<br />

$$ Gain (X_k, X_{k+1} ) = Entropy(X_k) -Entropy(X_k, X_{k+1})$$

$$ Gain  = \; Entropía \;del\; nodo\; padre - Promedio\; de \;entropía \;de \;los \;nodos\; hijos $$

<!-- <br />

<center>
![](images/infogain.png) </center>

<br /> -->

Mientras mas alta la ganancia de información (hasta llegar a 1), mejor es la separación.

<!--

**Error de clasificación**


$$(\sum_{x_{i} \epsilon R_{m}}I(y_{i} \ne k))/N_{m}$$
-->
<br />

<center>
![Medidas de impureza para una clasificación en dos grupos.](images/ImpurityMeasures.png)
</center>

<br />


## Ahora un ejemplo en R


**Gini Index**

```{R}
gini_process <-function(classes,splitvar = NULL){
  #Assumes Splitvar is a logical vector
  if (is.null(splitvar)){
    base_prob <-table(classes)/length(classes)
    return(1-sum(base_prob**2))
  }
  base_prob <-table(splitvar)/length(splitvar)
  crosstab <- table(classes,splitvar)
  crossprob <- prop.table(crosstab,2)
  No_Node_Gini <- 1-sum(crossprob[,1]**2)
  Yes_Node_Gini <- 1-sum(crossprob[,2]**2)
  return(sum(base_prob * c(No_Node_Gini,Yes_Node_Gini)))
}
```

Cálculo para los datos de Iris.
```{R}
data(iris)
gini_process(iris$Species) #0.6667
gini_process(iris$Species,iris$Petal.Length<2.45) 
gini_process(iris$Species,iris$Petal.Length<5) 
gini_process(iris$Species,iris$Sepal.Length<6.4) 
```

**Entropy**
```{R}
info_process <-function(classes,splitvar = NULL){
  #Assumes Splitvar is a logical vector
  if (is.null(splitvar)){
    base_prob <-table(classes)/length(classes)
    return(-sum(base_prob*log(base_prob,2)))
  }
  base_prob <-table(splitvar)/length(splitvar)
  crosstab <- table(classes,splitvar)
  crossprob <- prop.table(crosstab,2)
  No_Col <- crossprob[crossprob[,1]>0,1]
  Yes_Col <- crossprob[crossprob[,2]>0,2]
  No_Node_Info <- -sum(No_Col*log(No_Col,2))
  Yes_Node_Info <- -sum(Yes_Col*log(Yes_Col,2))
  return(sum(base_prob * c(No_Node_Info,Yes_Node_Info)))
}
```

Ejecutando para la base de Iris.
```{R}
data(iris)
info_process(iris$Species) #1.584963
info_process(iris$Species,iris$Petal.Length<2.45)
info_process(iris$Species,iris$Petal.Length<5) 
info_process(iris$Species,iris$Sepal.Length<6.4) 
```


**Information Gain.**

```{R}
library(FSelectorRcpp)
IG.FSelector <- information_gain(Species ~ ., data=iris)
IG.FSelector
```

Veamos una forma gráfica de realizar el registro del como se llena a cabo las particiones del espacio,este registro es mediante un árbol de decisiones y el corte se realiza mediante alguno de los criterios que acabamos de presentar. Veámos mediante un ejemplo cómo crear el árbol. Imaginemos que se tienen el dataset de la figora del lado izquierdo y que deseamos seccionarla solamente con lineas horizontales o verticales, de tal forma, que al terminar de partición los elementos dentro de cara región sean tan homogéneos como sea posible.  

<br />

Se comienza con una línea horizontal a altura 60, ese será nuestro primer corte o $split$. En términos del árbol (la figura en la parte derecha), esta partición crea dos caminos que divide a los datos entre aquellos que están por debajo de 60 y los que no. Nótese que la partición se llevó a cabo en la variable $X_{2}$. 

<br />

<center>
![](images/1.png){width=400px} ![](images/2.png){width=400px}
</center>

<br />

Nuestro siguiente paso será realizar un corte vertical en el valor de $X_{1}=50$ pero por arriba de  $X_{2}=60$. Con esto logramos separar los rojos de los verdes en la parte superior del gráfico. En términos del árbol de decisión, esto queda registrado en la rama de *no*, dado que no estamos tomando los valores menores a 60 en $X_{2}$. Por otro lado, al menos en nuestro hipotético ejemplo, hemos logrado separar perfectamente los conjuntos rojo y verde, de donde, en el árbol esto se debe de representar como nodos terminales en verde y rojo.

<br />

<center>
![](images/3.png){width=400px} ![](images/4.png){width=400px}
</center>

<br />

Sin embargo, nuestro trabajo no ha terminado, la parte inferior del gráfico todavía requiere trabajo para poder separar todos los grupos, asi que, ahora realizaremos un corte vertical en el valor de $X_{1}=70$. Con ello, hemos logrado aislar los puntos rojos del lado izquierdo y en términos del árbol de decisión esto queda registrado de la siguiente forma: dado que el corte es para valores de $X_{2}<60$ tenemos que utilizar la rama izquierda del árbol. El $split$ que realizamos fue en el valor de 70, por ello el nodo lo podemos etiquetar como $X_{1}<70$ y como para los valores menores a 70 se clasificó perfectamente al grupo rojo, se pone un nodo terminal de ese color. 

<br />

<center>
![](images/5.png){width=400px} ![](images/6.png){width=400px}
</center>

<br />

Casi hemos terminado. El últimmo corte para terminar de separar todos los grupos de nuestro $dataset$ debe de ser para los datos menores de $X_{2}<60$, pero mayores que $X_{1}<70$ y debe de ser un corte horizontal que separe los datos verdes y rojos, este corte debe de ser en $X_{2}=20$. En el árbol tenemos que tomar la rama $X_{1}<70$ de ahí tomamos la rama de $no$ y formamos un nuevo nodo con la nueva partición, el cual va a estar etiquetado con $X_{2}<20$. Como esa partición también logra separar perfectamente al conjunto verde del rojo, entonces ponemos nodos terminales con esos colores.

<br />

<center>
![](images/7.png){width=400px} ![](images/8.png){width=400px}
</center>

<br />







# Datos

Problema: Manufacturera desea encontrar que sector de mercado tiene mejor respuesta al lanzamiento de su high-end SUV . Información de usuarios en redes sociales. Las variables observados son:

* ID. de Usuario.
* Genero.
* Salario estimado.
* Usuario compro o no SUV.


*Importing the dataset


```{R}
library(foreign)
dataset = read.csv('data/Social_Network_Ads.csv')
dataset = dataset[3:5]
head(dataset)
```

Ahora un perqueño Pre-Procesamiento: Cambiar el formato como factor

```{R}
dataset$Purchased = factor(dataset$Purchased, levels = c(0, 1))
```


Separando la base de datos en entrenamiento y prueba con proporción del 75%

```{R}
library(caTools)
set.seed(123)
split = sample.split(dataset$Purchased, SplitRatio = 0.75)
training_set = subset(dataset, split == TRUE)
test_set = subset(dataset, split == FALSE)
```

Las variables deben de estar en la misma escala, o almenos una escala comparable.

```{R}
training_set[-3] = scale(training_set[-3])
test_set[-3] = scale(test_set[-3])
head(test_set[-3])
```


Se ajusta un árbol de decisiones al conjunto de entrenamiento.
```{R}
library(rpart)
classifier = rpart(formula = Purchased ~ .,
                   data = training_set)
```

Con lo anterior, se puede entonces tener una predicción sobre el conjunto de prueba.

```{R}
y_pred = predict(classifier, newdata = test_set[-3], type = 'class')
y_pred
```

Como medida de "ajuste" creamos la Matríz de Confusión.
```{R}
cm = table(test_set[, 3], y_pred)
cm
```

Nota: R tiene un método para evadir Overfitting cuando se utilizan parámetros Default. 

Gráfico de los resultados. Observe que ésta imágen proviene solo del conjunto de entrenamiento.

```{R} 
library(ElemStatLearn)
set = training_set
X1 = seq(min(set[, 1]) - 1, max(set[, 1]) + 1, by = 0.01)
X2 = seq(min(set[, 2]) - 1, max(set[, 2]) + 1, by = 0.01)
grid_set = expand.grid(X1, X2)
colnames(grid_set) = c('Age', 'EstimatedSalary')
y_grid = predict(classifier, newdata = grid_set, type = 'class')
plot(set[, -3],
     main = 'Decision Tree Classification (Training set)',
     xlab = 'Age', ylab = 'Estimated Salary',
     xlim = range(X1), ylim = range(X2))
contour(X1, X2, matrix(as.numeric(y_grid), length(X1), length(X2)), add = TRUE)
points(grid_set, pch = '.', col = ifelse(y_grid == 1, 'springgreen3', 'tomato'))
points(set, pch = 21, bg = ifelse(set[, 3] == 1, 'green4', 'red3'))
```

<br />

Ahora vamos a graficar los resultados aplicando al conjunto de Prueba. Hay que notar que el rectángulo rojo que sale en la parte derecha. Para el conjunto de datos de entrenamiento, el rectángulo tiene utilidad en tanto esta capturando algunos puntos rojos. Por el otro lado, cuando esa misma separación se aplica al conjunto de prueba, vemos que no es tan útil. Esto podría ser un indicio de sobreajuste.

<br />

```{R}
library(ElemStatLearn)
set = test_set
X1 = seq(min(set[, 1]) - 1, max(set[, 1]) + 1, by = 0.01)
X2 = seq(min(set[, 2]) - 1, max(set[, 2]) + 1, by = 0.01)
grid_set = expand.grid(X1, X2)
colnames(grid_set) = c('Age', 'EstimatedSalary')
y_grid = predict(classifier, newdata = grid_set, type = 'class')
plot(set[, -3], main = 'Decision Tree Classification (Test set)',
     xlab = 'Age', ylab = 'Estimated Salary',
     xlim = range(X1), ylim = range(X2))
contour(X1, X2, matrix(as.numeric(y_grid), length(X1), length(X2)), add = TRUE)
points(grid_set, pch = '.', col = ifelse(y_grid == 1, 'springgreen3', 'tomato'))
points(set, pch = 21, bg = ifelse(set[, 3] == 1, 'green4', 'red3'))
```

<br />

La gráfica del árbol es la siguiente:

```{R}
plot(classifier)
text(classifier)
```

<br />

































<br />

# Referencias.

1. Hastie, Trevor. Tibshirani, Robert. Friedman, Jerome. $\textit{The Elements of Statistical Learning: Data Mining, Inference and Prediction}$.California: Springer Series in Statistics.

2. Maimon, Oded. & Rokach, Lior. $\textit{Data Mining and Knowledge Discovery Handbook}$. Springer Science+Business Media, 2010.

3. Bishop, Christopher. $\textit{Pattern Recognition and Machine Learning}$. Cambridge: Springer, 2006.

4. Super Data Science.
www.superdatascience.com

5. Curso de la Universidad de Stanford en Estadística, Stats202.
https://web.stanford.edu/class/stats202/content/

6. Learn by Making Site.
http://www.learnbymarketing.com/481/decision-tree-flavors-gini-info-gain/

7. University of Washington, Computer Science Department.
https://homes.cs.washington.edu/~shapiro/EE596/notes/InfoGain.pdf

8. University of Notre Dame. CSE40647
https://www3.nd.edu/~rjohns15/cse40647.sp14/www/content/lectures/23%20-%20Decision%20Trees%202.pdf



<br />





