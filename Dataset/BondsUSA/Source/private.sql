
select  cusip, businessunit, instrumentissuer, productgroup10k, l3producttype, l4productsubtype, l5product, liquiditytier, counterparty, termgrouping3, termgrouping4
 from [secfund].vw_SecuredFunding 
where datasource='LOANET' and 
cusip in (
SELECT distinct [ID_CUSIP]
FROM [jds].[SECURITY_BBG_ATTR] WHERE COUNTRY_ISO='US'
AND MARKET_SECTOR_DES='Corp'
) 
group by cusip, businessunit, instrumentissuer, productgroup10k, l3producttype, l4productsubtype, l5product,
 liquiditytier, counterparty, termgrouping3, termgrouping4

