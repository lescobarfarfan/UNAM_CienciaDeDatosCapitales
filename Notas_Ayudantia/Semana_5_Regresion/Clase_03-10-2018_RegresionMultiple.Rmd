---
title: "Regresion Lineal"
author: "Luis Escobar"
date: "02/10/2018"
output:
  pdf_document:
    extra_dependencies: ["multicol", "bm"]
  html_document: default
bibliography: referencias.bib
---

<style>
body {text-align:justify}
</style/

```{r setup, include=FALSE}
suppressMessages(library(MASS))
suppressMessages(library(ISLR))
suppressMessages(library(tidyverse))
library(knitr)
library(matlib)
library(scatterplot3d)
knitr::opts_chunk$set(echo = TRUE)
```



# <font color="green">Regresión Lineal</font>

La regresión lineal, a pesar de ser relativamente sencilla, continua siendo
una de las herramientas más socorridas para hacer análisis y modelaje. A pesar
de ser una técnica estadística con un considerable tiempo de vida, ha sido
incorporada con gran éxito en los relativamente nuevos campos de aprendizaje
estadístico (statistical learning), aprendizaje máquina (machine learning), y,
muy recientemente, la ciencia de datos (data science); y la razón para ser
incorporada no carece de mérito, pues la regresión lineal cumple cabalmente con
el cometido de todas las técnicas utilizadas en los campones mencionados:
la extracción de conocimiento de un conjunto de datos.

Más aún, dado que la regresión lineal forma parte de los métodos de
*aprendizaje supervisado*, ademásde extraer conocmiento,nos ayuda a modelar
nuestros para hacer predicciones e inferencia sobre el proceso subyacente
presente en nuestros datos.


## <font color="green">Regresion lineal simple</font>
Recordando brevemente la regresión lineal simple, se asume que existe una
relación lineal entre dos variables, $X$ y $Y$, donde a $Y$ se le conoce como la
variable dependiente, y a $X$ se le conoce como la variable independiente o
regresor. Las siguientes definiciones pueden ser consultadas con mucho mayor
detalle en @ISLR2014, @Neter1996 y @Johnson1988

Dada la linealidad entre solo dos variables, dicha relación puede ser
explicada a través de una recta definida como:

\begin{equation}
  Y = \beta_{0} + \beta_{1}X
\end{equation}

Dado que de entrada no conocemos los valores reales de $x$ y $y$, lo mejor que
podemos hacer es obtener valores estimados para los parámetros de nuestra
recta, es decir, lo mejor que podemos obtener es una aproximación de la forma

\begin{equation}
  \hat{y} = \hat{\beta_{0}} + \hat{\beta_{1}}x
\end{equation}

donde el simbolo de "gorro" índica que son valores para los que sólo tenemos
aproximaciones de los valores reales.

Para obtener los valores estimado de los parámetros $\beta_{0}$ y $\beta_{1}$
contamos con un conjunto de datos con $n$ observaciones, para las cuales se
cuenta con un valor $y_{i}$, variable dependiente, y un valor $x_{i}$ que sirve
como regresor. Con esas $n$ observaciones, se tiene como objetivo obtener
valores para los parámetros beta tales que:

\begin{equation}
  y_{i} = \hat{\beta_{0}} + \hat{\beta_{1}}x_{i}
\end{equation}

es decir, que la línea resultante esté lo más *cerca* posible de los $n$ puntos
de nuestro conjunto de datos. Es claro que al obtener los estimados de los
parámetros de una *muestra* de una población, no es posible obtener los valores
exactos de los parámetros, por lo que cualquier modelo obtenido con regresión
lineal es propenso a tener desviaciones (o residuales) de los valores reales $y_{i}$.
Dichos errores son importantes para medir la calidad de un ajuste, definimos el
residual para la observación $i$ como:

\begin{equation}
  e_{i} = y_{i} - \hat{y_{i}}
\end{equation}

De manera intuitiva, los errores se pueden pensar como valores representativos
que dan cuenta de *otros* regresores que son importantes para la predicción de
$Y$ pero que no están contemplados en nuestro conjunto de datos. Dentro de las
cosas que se pueden verificar para ver qué tan bueno es un ajuste, se tiene que
verificar que los errores cumplan dos supuestos:

* $e_{i} \perp e_{j} \:\: \forall \: i \neq j$
* $e_{i} \sim N(0, \sigma^{2})$

De las dos ecuaciones anteriores obtenemos que
\begin{equation}
  e_{i} = (y_{i} - \hat{\beta_{0}} - \hat{\beta_{1}}x_{i})
\end{equation}

El concepto de cercanía puede ser sumamente ambiguo, por lo que en este caso
tomaremos la cercanía de acuerdo al criterio de *mínimos cuadrados* (Least Squares).
En este caso en particular, se utilizar un método llamado *Ordinary Least Squares*,
que consiste en minimizar la suma de los cuadrados de los residuales (Residual
Sum of Squares) definida como:

\begin{equation}
  RSS = e_{1}^{2} + e_{2}^{2} + \dots + e_{n}^{2}
\end{equation}

o lo que es lo mismo, obtener los valores de $\beta_{1}$ y $\beta_{0}$ que
minimicen

\begin{equation}
  RSS = (y_{1} - \hat{\beta_{0}} - \hat{\beta_{1}}x_{1}) + 
        (y_{2} - \hat{\beta_{0}} - \hat{\beta_{1}}x_{2}) + \dots +
        (y_{n} - \hat{\beta_{0}} - \hat{\beta_{1}}x_{n}) = \sum_{i=1}^{n}(y_{i} - \hat{\beta_{0}} - \hat{\beta_{1}}x_{i})^{2}
\end{equation}

La expresión más a la derecha de la ecuación anterior es la función que conocemos
y de la que derivamos las expresiones finales de los estimadores de los
parámetros del intercepto y la pendiente de la recta que mejor se acomoda en
nuestros datos. Sólo para refrescar, el procedimiento que se sigue es el mismo
que para cualquier otro problema estándar de minimización:

1. Parciales respecto a cada parámetro
2. Igualar a cero cada parcial
3. Resolver el sistema de ecuaciones resultante

El sistema de ecuaciones resultante es importante, y recibe el nombre de
**ecuaciones normales**, que, para el caso de la regresión lineal simple tiene
la siguiente forma:

\begin{equation}
  \begin{aligned}
    \sum_{i=1}^{n}y_{i} = n\hat{\beta_{0}} + \hat{\beta_{1}}\sum_{i=1}^{n}x_{i} \\
    \sum_{i=1}^{n}x_{i}y_{i} = \hat{\beta_{0}}\sum_{i=1}^{n}x_{i} + \hat{\beta_{1}}\sum_{i=1}^{n}x_{i}^{2}
  \end{aligned}
\end{equation}

resolviendo las ecuaciones anteriores para las betas, obtenemos los siguientes
estimadores:


\begin{equation}
  \hat{\beta_{1}} = \frac{\sum_{i = 1}^{n}(x_{i}-\overline{x})(y_{i} - \overline{y})}{\sum_{i = 1}^{n}(x_{i} - \overline{x})^{2}}
\end{equation}

y $\beta_{0}$ se obtiene como:

\begin{equation}
  \hat{\beta_{0}} = \overline{y} - \hat{\beta_{1}}\overline{x}
\end{equation}

### <font color="gree">Representación matricial de la regresión lineal simple</font>
Todas las definiciones anteriores pueden ser reescritas en terminos matriciales,
lo cual resulta muy conveniente al momento de extender el modelo para permitir
más de un regresor. Dado que tenemos $n$ observaciones con $n$ valores de $Y$
y $n$ valores de $X$, tenemos $n$ ecuaciones de la forma

\begin{equation}
  \hat{y_{i}} = \hat{\beta_{0}} + \hat{\beta_{1}}x_{i} + e_{i}
\end{equation}

o lo que es lo mismo, un sistema de ecuaciones de la forma:

\begin{equation}
  \begin{aligned}
    y_{1} = \hat{\beta_{0}} + \hat{\beta_{1}}x_{1} + e_{1} \\
    y_{2} = \hat{\beta_{0}} + \hat{\beta_{2}}x_{1} + e_{2} \\
          \vdots \\
    y_{n} = \hat{\beta_{0}} + \hat{\beta_{1}}x_{n} + e_{n}
  \end{aligned}
\end{equation}

Con la vista anterior, es claro que podemos extraer componentes de nuestro sistema
en forma matricial:

  \begin{align}
    \textbf{Y} &=
    \begin{bmatrix}
      y_{1} \\
      y_{2} \\
      \vdots \\
      y_{n} \
    \end{bmatrix}
  \end{align}\break

  \begin{align}
    \textbf{X} &=
    \begin{bmatrix}
      1 & x_{1} \\
      1 & x_{2} \\
      \vdots & \vdots \\
      1 & x_{n} \
      \end{bmatrix}
  \end{align}\break

  \begin{align}
    \beta &=
    \begin{bmatrix}
      \beta_{0} \\
      \beta_{1} \
      \end{bmatrix}
  \end{align}\break

  \begin{align}
    \textbf{e} &=
    \begin{bmatrix}
      e_{i} \\
      e_{2} \\
      \vdots \\
      e_{n} \
    \end{bmatrix}
  \end{align}

Así, podemos reescribir nuestro sistema anterior como:

\begin{equation}
  \textbf{Y} = \textbf{X}\beta + \textbf{e}
\end{equation}

Es fácil ver que con la formulación anterior, los valores estimados de $Y$ los
podemos calcular como:

\begin{equation}
  \hat{\textbf{Y}} = \textbf{X}\hat{\beta}
\end{equation}

Las ecuaciones normales también las podemos representar de manera matricial de
la siguiente forma:

\begin{equation}
 \textbf{X}^{'}\textbf{X}\beta = \textbf{X}^{'}\textbf{Y}
\end{equation}

Si despejamos el valor de $\beta$ de la ecuación anterior, obtendremos la
forma matricial de los estimadores para los parámetros beta:

\begin{equation}
 \hat{\beta} = (\textbf{X}^{'}\textbf{X})^{-1}\textbf{X}^{'}\textbf{Y}
\end{equation}

### <font color="green">Ejemplo</font>
Véamos un ejemplo rápido de cómo obtener en R los valores de $\beta$ definidos.
Utilizaremos el data set "Advertising", donde cada renglón presenta el gasto en
miles de dólares de una empresa en tres medios de comunicación: televisión,
radio y periódicos. Además, hay una cuarta columna "sales", que dice el número
de ventas del producto que ofrece la compañia en miles de unidades.

```{r}
# Cargamos los datos
datos <- "data/Advertising.csv" %>% read_csv(col_names = T, progress = F)
datos <- datos %>% select(-X1)
datos %>% head()
```

Veamos una visualización de los datos, para empezar a inferir si existe
una relación aproximadamente lineal entre los gastos en publicidad y el número
de ventas registradas:

```{r}
# Graficamos
plot(datos)
```

Sólo nos interesan las gráficas en el último renglon: vemos que la relación con
las ventas más similar a una recta se da con el gasto en televisión. Para efectos
ilustrativos de la regresión lineal simple, haremos los cálculos para estimar
las betas tomando únicamente la columna TV.

```{r}
# Seleccionamos las columnas necesarias
datos_tv <- datos %>% select(sales, TV)
datos_tv %>% head()
```

Procedemos de dos formas: usando la función de R para regresión lineal, y
estimando los parámetros "a mano" usando la representación matricial.

#### <font color="gree">Con la función lm</font>

```{r}
#Primero, estimamos los parámetros con la función lm
ajuste1 <- lm(formula = sales ~ TV, data = datos_tv)

# Imprimimos los principales resultados del modelo
summary(ajuste1)

```
Vemos que los valores estimados para $\beta_{0}$ y $\beta_{1}$ son 7.0325 y
0.0475 respectivamente. ¿Cómo interpretamos esos resultados? Recordemos que las
unidades de $Y$ (sales) en este caso son miles de unidades, mientras que en el caso de
$X$ (TV) son miles de dólares; así, podemos decir que un aumento de \$1,000
dólares en el gasto en publicidad en la televisión tiene como consecuencia
un aumento de 475 unidades vendidas ($\beta_{0}$ por su parte nos dice que sin
necesidad de hacer algún gasto en publicidad en TV, las ventas son aproximadamente
de 7,032 unidades).

```{r}
plot(datos_tv$TV, datos_tv$sales)
abline(lm(formula = sales ~ TV, data = datos_tv), col="blue")
```


#### <font color="green">Con la representación matricial</font>
Recordemos que con la representación matricial nuestro estimador se ve como:

\begin{equation}
 \hat{\beta} = (\textbf{X}^{'}\textbf{X})^{-1}\textbf{X}^{'}\textbf{Y}
\end{equation}

```{r}
# Creamos lo necesario, primero, es importante separar nuestros datos y agregar
# la columna de unos a X, que corresponde con el valor correspondiente al
# coeficiente beta cero.
Y <- datos_tv$sales

X <- datos_tv$TV
X <- cbind(rep(1, length(Y)), X)
colnames(X) <- c("BetaCero", "TV")

head(Y)
head(X)

```

Obtenemos los valores del vector $\beta$

```{r}
aux1 <- t(X) %*% X
aux2 <- t(X) %*% Y

aux3 <- inv(aux1)

beta <- aux3 %*% aux2
cat(paste0("beta0 = ", round(beta[1], 4), "\n", "beta1 = ", round(beta[2], 4)))
```

Como vemos, obtenemos los mismo resultados. Así, podemos obtener los valores
estimados de $Y$ para poner después obtener los residuales.

```{r}
# Calculamos los residuales
Y_estim <- X %*% beta
Y_estim <- as.vector(Y_estim)
print(head(Y_estim))

residuales <- Y - Y_estim
head(residuales)
```

Se comportan de acuerdo a los supuestos del modelo?

Primero, tomando los residuales de la función *lm*
```{r}
residuales_lm <- ajuste1$residuals
mean(residuales_lm) # Casi cero
sd(residuales_lm) # No tiene porque ser uno

hist(residuales_lm, freq = F, breaks = 30)
curve(dnorm(x, mean(residuales_lm), sd(residuales_lm)), add = T, col = "red")

# Una prueba de normalidad
shapiro.test(residuales_lm) # No se rechaza normalidad

```


Ahora, con los residuales obtenidos "a pata":
```{r}
mean(residuales)
sd(residuales)

hist(residuales, freq = F, breaks = 30)
curve(dnorm(x, mean(residuales), sd(residuales)), add = T, col = "red")

# Una prueba de normalidad
shapiro.test(residuales) # No se rechaza normalidad

```

**¿Por qué la media es tan distinta de la media de los residuales obtenidos con lm?**


## <font color="green">Regresión Lineal Múltiple</font>
Como mencionamos, utilizando la representación matricial resulta sencillo
hacer la extensión al modelo de regresión lineal múltiple. En la vidar real es
mucho más común encontrarse con conjuntos de datos con un gran número de variables,
por lo que resulta natural extender al modelo múltiple, donde permitimos que
haya más de un regresor, es decir, ahora podemos tener un vector de variables
$X = [X_{1}, \dots, X_{p}]$, y deseamos saber si también existe un una relación
aproximadamente linel entre los $p$ regresores y la variable dependiente $Y$.

Es posible realizar un ajuste simple con cada uno de los $p$ regresores, sin
embargo, las $p$ variables "actuan" simultáneamente sobre el proceso generador
de la variable respuesta $Y$ ¿cómo conciliamos la información  de las $p$
regresiones? A final de cuentas, queremos obtener valores estimados de $Y$ usando
la información de las $p$ variables.

Así, definimos el modelo de *regresión múltiple* como:

\begin{equation}
  Y = \beta_{0} + \beta_{1}x_{1} + \beta_{2}x_{2} + \dots + \beta_{p}x_{p} + e
\end{equation}

Aquí, dado que tenemos más de dos variables beta relacionadas con un regresor,
la interpretación de los parámetros $\beta_{i}$ se extiende, y se considera que
$\beta_{j}$ representa el cambio promedio en $Y$ dado el aumento en una unidad
en su regresor asociado $X_{j}$, **manteniendo el resto de los regresores fijos**
(@ISLR2014).

En forma matricial, nuestro problema de regresión múltiple se descompone en:

  \begin{align}
    \textbf{Y} &=
    \begin{bmatrix}
      y_{1} \\
      y_{2} \\
      \vdots \\
      y_{n} \
    \end{bmatrix}
  \end{align}\break

  \begin{align}
    \textbf{X} &=
    \begin{bmatrix}
      1 & x_{11} &  x_{12} & \dots & x_{1,p-1}\\
      1 & x_{21} & x_{22} & \dots & x_{2,p-1}\\
      \vdots & \vdots & \vdots & \ddots & \vdots \\
      1 & x_{n1} & x_{n2} & \dots & x_{n,p-1} \
      \end{bmatrix}
  \end{align}\break

  \begin{align}
    \beta &=
    \begin{bmatrix}
      \beta_{0} \\
      \beta_{1} \\
      \vdots \\
      \beta_{p-1}
      \end{bmatrix}
  \end{align}\break

  \begin{align}
    \textbf{e} &=
    \begin{bmatrix}
      e_{i} \\
      e_{2} \\
      \vdots \\
      e_{n} \
    \end{bmatrix}
  \end{align}

Así, definimos el modelo de la misma forma que para la regresión simple:

\begin{equation}
  \textbf{Y} = \textbf{X}\beta + \textbf{e}
\end{equation}

Y, sin sorpresas, el estimador del vector $\beta$ tiene la misma forma que
en la regresión simple:

\begin{equation}
 \hat{\beta} = (\textbf{X}^{'}\textbf{X})^{-1}\textbf{X}^{'}\textbf{Y}
\end{equation}

Retomando el ejemplo anterior, vemos que los datos completos contienen dos
variables más: gasto en publicidad en radio y en periódicos. Analizaremos
ahora el efecto de los tres regresores en el número de ventas.

#### <font color="green">Con la función lm</font>

```{r}
# Ajustamos
ajuste2 <- lm(formula = sales ~ TV + radio + newspaper, data = datos)
summary(ajuste2)
```

El efecto de agregar nuevos regresores es claro: el intercepto que obtenemos
es mucho más pequeño que el que se tiene para la regresión simple con gatos en
TV, el coeficiente de TV es muy similar, y el coeficiente de la publicidad en
radio es el más grande: por cada \$1,000 pesos de publicidad en radio, el
aumento en ventas es de 189 unidades del producto. Por su parte, la publicidad
en periódico es incluso negativa, sin embargo, es un coefciente no significativo
para el modelo (p-value, más adelante veremos inferencia sobre los parámetros
y sobre los valores estimados). En el caso de regresión múltiple,
es un *hiper plano* el que estamos ajustando a los datos.

```{r}
# Podemos obtener un scatter plot en tres dimensiones (dos regresores)
# Tomemos los dos significativos, TV y Radio.
s3d <- scatterplot3d(datos$TV, datos$radio, datos$sales,
                     highlight.3d = T, type = "h")
s3d$plane3d(lm(sales ~ TV + radio, data = datos))

```


#### <font color="green">Con la forma matricial</font>

```{r}
# Creamos lo necesario, primero, es importante separar nuestros datos y agregar
# la columna de unos a X, que corresponde con el valor correspondiente al
# coeficiente beta cero.
Y_f <- datos$sales

X_f <- datos[, c("TV", "radio", "newspaper")]
X_f <- cbind(rep(1, length(Y_f)), X_f)
colnames(X_f) <- c("BetaCero", "TV", "radio", "newspaper")
X_f <- as.matrix(X_f)

head(Y_f)
head(X_f)
```

Obtenemos los estimados de $\beta$

```{r}
aux1 <- t(X_f) %*% X_f
aux2 <- t(X_f) %*% Y_f

aux3 <- inv(aux1)

beta2 <- aux3 %*% aux2
cat(paste0("beta0 = ", round(beta2[1], 4), "\n", "beta1 = ", round(beta2[2], 4),"\n","beta2 = ", round(beta2[3], 4),"\n", "beta4 = ", round(beta2[4], 4)))
```

Hay problemas de redondeo. Ahora, obtenemos los residuales
```{r}
# Calculamos los residuales
Y_estim_f <- X_f %*% beta2
Y_estim_f <- as.vector(Y_estim_f)
print(head(Y_estim_f))

residuales_f <- Y_f - Y_estim_f
head(residuales_f)
```

¿Los residuales se comportan bien?

Primero, de la función lm
```{r}
residuales_lm_f <- ajuste2$residuals
mean(residuales_lm_f)
sd(residuales_lm_f)

hist(residuales_lm_f, freq = F, breaks = 30)
curve(dnorm(x, mean(residuales_lm_f), sd(residuales_lm_f)), add = T, col = "red")

```


Ahora, con los residuales "a pata".

```{r}
mean(residuales_f)
sd(residuales_f)

hist(residuales_f, freq = F, breaks = 30)
curve(dnorm(x,mean(residuales_f), sd(residuales_f)), add = T, col="red")

```



# <font color="green">¿Qué tan bueno es un ajuste?</font>
Continuará...


# <font color="green">Referencias</font>