from lxml import html  
import requests
from time import sleep
import json
import argparse
from collections import OrderedDict
from time import sleep
import pandas as pd

asset_universe=pd.read_csv('tickers.csv')
#6000 to 20000
nstart=20000
nend=40000
tickers= list(asset_universe['Ticker'][nstart:nend])

for ticker in tickers:
    url = "http://finance.yahoo.com/quote/%s?p=%s"%(ticker,ticker)
    response = requests.get(url, verify=False)
    print ("Parsing %s"%(url))
    sleep(4)
    parser = html.fromstring(response.text)
    summary_table = parser.xpath('//div[contains(@data-test,"summary-table")]//tr')
    summary_data = OrderedDict()
    other_details_json_link = "https://query2.finance.yahoo.com/v10/finance/quoteSummary/{0}?formatted=true&lang=en-US&region=US&modules=summaryProfile%2CfinancialData%2CrecommendationTrend%2CupgradeDowngradeHistory%2Cearnings%2CdefaultKeyStatistics%2CcalendarEvents&corsDomain=finance.yahoo.com".format(ticker)
    summary_json_response = requests.get(other_details_json_link)
    try:
            print(1)
            json_loaded_summary =  json.loads(summary_json_response.text)
            with open('output/' + ticker + '.json', 'w') as outfile:
                json.dump(json_loaded_summary, outfile)

    except:
            print ("Failed to parse json response")


